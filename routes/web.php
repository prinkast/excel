<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('/', 'HomeController@index')->name('instructor');
// Route::get('/create', 'HomeController@create')->name('instructor');


Route::get('/', function () {
    return view('auth/login');
});

Route::post("emailvalidationcheck","FunctionController@emailValidate");
Auth::routes();
Route::get('/dashboard','admin\UsersController@dashboard')->name('dashboard');
	Route::post('/dashboard','admin\UsersController@upload_file')->name('dashboard');
Route::group(['middleware' => ['Admin']], function () {
	Route::get('/dashboard','admin\UsersController@dashboard')->name('dashboard');
	Route::post('/dashboard','admin\UsersController@upload_file')->name('dashboard');
	
});  



Route::group(['middleware' => ['CheckPermission']], function () {	
	
	Route::match(['get','post'],'/emails','EmailsController@index')->name('dashboard');
    Route::match(['get','post'],'/notice','NoticeController@index')->name('dashboard');
    Route::match(['get','post'],'/calendar','CalendarController@index')->name('dashboard');
	Route::match(['get','post'],'/accounts','UsersController@accounts')->name('dashboard');
	Route::match(['get','post'],'/settings','UsersController@settings')->name('dashboard');
	Route::match(['get','post'],'/role-permission','PermissionsController@index')->name('dashboard');


	/* manage documents. */
	Route::match(['get','post'],'/document-list','DocumentController@index')->name('dashboard');
   
	Route::match(['get'],'/instructor/view/{id}','admin\UsersController@show')->name('instructor-view');

	/* manage mail*/
	Route::post('/checkemail', 'admin\UsersController@EmailExists')->name('check-email');
	Route::get('/change-password/{id}', function($id){
		return view('change_password',['id'=>$id]);
	})->name('change-password');

	Route::post('/update-password', 'admin\UsersController@updatePassword')->name('update-password');

	// instructor routes
	Route::match(['get','post'],'/instructor/{param}/{param2?}','InstructorController@index')->name('instructor');
	// category routes
	Route::match(['get','post'],'/category/{param}/{param2?}','admin\CategoryController@index')->name('category');
	// course routes
	Route::match(['get','post'],'/course/{param}/{param2?}','admin\CourseController@index')->name('course');
	// programs routes
	Route::match(['get','post'],'/program/{param}/{param2?}','admin\ProgramController@index')->name('program');
	// save route
	Route::match(['post'],'/ajax/save','FunctionController@Save')->name('ajax/save');
	
	Route::match(['post'],'/assign/program/{param}','admin\ProgramController@AssignProgram')->name('program-assign');
	Route::match(['post'],'/delete','FunctionController@delete')->name('delete');
	
	Route::match(['get','post'],'/student/{param}/{param2?}','StudentController@index')->name('student');
	


});  

Route::group(['middleware' => ['User']], function () {
    Route::get('/demo', 'AdminController@demo');
    Route::get('/instructor','InstructorController@dashboard');
    
}); 

Route::post('/email-existance', 'FunctionController@emailExistance');

Route::get('/404', function () {
	return View::make('errors.404');
});
Route::post('/remove/doc','FunctionController@RemoveDoc');
Route::post('/getData','FunctionController@GetData');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
