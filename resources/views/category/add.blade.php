@extends('layouts.form')
@section('content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

<!-- begin:: Subheader -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                Dashboard </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
            </div>
        </div>
    </div>
    <!-- end:: Subheader -->
    <!-- sidebar and header ends here -->

    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid mt-5">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__body kt-portlet__body--fit">
                <!--begin: Datatable -->
                <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded" id="local_data" style="">
					<form class="kt-form" method="post" id="category_form">
						<div class="kt-portlet__body " >
							<div class="row">
                                <div class="col-md-7">
                                    <input type="hidden" name="category_id" value="{{isset($category['id'])? $category['id'] : ''}}">
                                    <input type="hidden" name="method" value="{{ isset($edit)?'update':'add'}}">
                                    <div class="row">
										<div class="form-group col-md-6">
											<label>Category Name</label>
											<input type="text" class="form-control" aria-describedby="nameHelp" placeholder="Enter Category Name" name="cat_name" value="{{isset($category['name'])? $category['name'] : ''}}">            
										</div>
                                        <div class="form-group col-md-6">
                                            <label>Time valid</label>
                                            <input type="text" class="form-control" aria-describedby="nameHelp" name="time" id="datepicker" value="{{isset($category['time_valid'])? $category['time_valid'] : ''}}">            
                                        </div>
										
                                        <div class="form-group col-md-6">
                                            <label for="exampleSelect1">Status</label>
                                            <select class="form-control" name="status">
                                                <option value="1" <?php echo (isset($category['status']) && $category['status'] =='1')?'selected' : '' ?>>Active</option>
                                                <option value="2" <?php echo (isset($category['status']) && $category['status'] =='2')?'selected' : '' ?>>Inactive</option>
                                                <option value="3" <?php echo (isset($category['status']) && $category['status'] =='3')?'selected' : '' ?>>Deleted</option>
                                            </select>
                                        </div>
                                    </div>
                                            
                                    <div class="kt-portlet__foot">
                                        <div class="kt-form__actions">
                                            <button type="submit" class="btn btn-primary" id="{{ isset($edit)?'update':'add'}}">Submit</button>
                                            <button type="reset" class="btn btn-secondary">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
						</div>
					</form>                                    
                </div>
                <!--end: Datatable -->
            </div>
        </div>
    </div>
@endsection
                    