@extends('layouts.grid')
@section('content')

        <div class="kt-portlet__body kt-portlet__body--fit">
            <!--begin: Datatable -->
            @if(session()->has('message'))
                {{ session()->get('message') }}
            @endif
            <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded" id="local_data" style="">
                <div style="display: block;background-color: #fff;padding: 25px;">
                    <a href="{{ route('category',['param'=>'create'])}}" type="button" class="add_btn">Add category</a>
                <table class="kt-datatable__table table-bordered" style="display: block;padding: 10px; min-height: 330px;width:100%;" id="category_table">
                    <thead class="kt-datatable__head">
                        <tr class="kt-datatable__row" style="left: 0px;">
                            
                            <th data-field="OrderID" class="kt-datatable__cell kt-datatable__cell--sort"><span style="width: 113px;">Sr no.</span></th>
                            <th data-field="Country" class="kt-datatable__cell kt-datatable__cell--sort"><span style="width: 113px;">Category name</span></th>
                            <th data-field="ShipDate" class="kt-datatable__cell kt-datatable__cell--sort"><span style="width: 113px;">Time valid</span></th>
                            <th data-field="Status" class="kt-datatable__cell kt-datatable__cell--sort"><span style="width: 113px;">Status</span></th>
                            <th data-field="Actions" data-autohide-disabled="false" class="kt-datatable__cell kt-datatable__cell--sort"><span style="width: 110px;">Actions</span></th>
                        </tr>
                    </thead>
                    <tbody class="kt-datatable__body" style="">
                        @php $i = 1; @endphp
                        @if(isset($categroyList))
                            @foreach($categroyList as $categroy)
                                <tr class="kt-datatable__row">
                                    <td class="kt-datatable__cell"><span style="width: 113px;">{{ $i++ }}</span></td>
                                    <td class="kt-datatable__cell"><span style="width: 113px;">{{ $categroy->name }}</span></td>
                                    <td class="kt-datatable__cell"><span style="width: 113px;">{{ $categroy->time_valid }}</span></td>
                                    <?php
                                        $object = new \App\library\functions;
                                    ?>

									<td class="kt-datatable__cell"><span style="width: 113px;">{{ $object->getStatus($categroy->status) }}</span></td>
									<td class="kt-datatable__cell">
									<div class="d-inline">
										<a class="" href="{{ route('category',['param'=>'edit','param2'=>$categroy->id])}}"><i class="kt-menu__link-icon flaticon-edit "></i>&nbsp;</a>

                                        <a class="" href="" style="display:none;"><i class="kt-menu__link-icon 
                                            flaticon-medical "></i>&nbsp;</a>
                                        <a class="" onclick="Delete('{{ $categroy->id }}')" href="javascript:void(0)"><i class="kt-menu__link-icon flaticon-delete"></i>&nbsp;</a>                           
                                    </div>   
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
                </div>
            </div>
@endsection