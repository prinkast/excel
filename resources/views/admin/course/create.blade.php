@extends('layouts.form')
@section('content')

    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid mt-5">
        <div class="kt-portlet kt-portlet--mobile">
          <h2 class="breadcrumb__title">
                {{@$title[0]}} </h2>
            <div class="kt-portlet__body kt-portlet__body--fit">
                <!--begin: Datatable -->
                <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded" id="local_data" style="">
                    <form class="kt-form" method="post" id="course_form">
                        <div class="kt-portlet__body " >
                            <div class="row">
                                <div class="col-md-7">
                                    <input type="hidden" name="course_id" value="{{isset($courseData['id'])? $courseData['id'] : ''}}">
                                    <input type="hidden" name="method" value="{{ isset($edit)?'course-update':'course-store'}}">
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="exampleSelect1">Select Program</label>
                                            <select class="form-control" name="program">
                                                @if(isset($programList))
                                                    @foreach ($programList as $program)
                                                        <option value="{{ $program['id'] }}">{{ $program['title']}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="exampleSelect1">Select Instructor</label>
                                            <select class="form-control" name="instructor">
                                                @if(isset($instructorList))
                                                    @foreach ($instructorList as $instructor)
                                                        <option value="{{ $instructor['id'] }}">{{ $instructor['first_name']}} {{ $instructor['last_name'] }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Course name</label>
                                            <input type="text" class="form-control" aria-describedby="nameHelp" name="course_name"  value="{{isset($courseData['name'])? $courseData['name'] : ''}}">            
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Duration</label>
                                            <input type="text" class="form-control" aria-describedby="nameHelp" name="duration"  value="{{isset($courseData['duration'])? $courseData['duration'] : ''}}">            
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Agency location</label>
                                            <input type="text" class="form-control" aria-describedby="nameHelp" name="agency" value="{{isset($courseData['agency_location_id'])? $courseData['agency_location_id'] : ''}}">            
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Start date</label>
                                            <input type="text" class="form-control" aria-describedby="nameHelp" name="start"value="{{isset($courseData['start_datetime'])? $courseData['start_datetime'] : ''}}" id="start">            
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>End date</label>
                                            <input type="text" class="form-control" aria-describedby="nameHelp" name="end" value="{{isset($courseData['end_datetime'])? $courseData['end_datetime'] : ''}}" id="end">            
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="exampleSelect1">Scope</label>
                                            <select class="form-control" name="scope">
                                                <option value="1" <?php echo (isset($courseData['private']) && $courseData['private'] =='1')?'selected' : '' ?>>Public</option>
                                                <option value="2" <?php echo (isset($courseData['private']) && $courseData['private'] =='2')?'selected' : '' ?>>Private</option>
                                            </select>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label for="exampleSelect1">Location</label>
                                             <input type="text" class="form-control" aria-describedby="nameHelp" name="location" value="{{isset($courseData['location'])? $courseData['location'] : ''}}">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Notes</label>
                                            <input type="text" class="form-control" aria-describedby="nameHelp" name="notes" value="{{isset($courseData['notes'])? $courseData['notes'] : ''}}">            
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="exampleSelect1">Permit File</label>
                                            <select class="form-control" name="permit">
                                                <option value="1" <?php echo (isset($courseData['permit']) && $courseData['permit'] =='1')?'selected' : '' ?>>Yes</option>
                                                <option value="2" <?php echo (isset($courseData['permit']) && $courseData['permit'] =='2')?'selected' : '' ?>>No</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Instructor notes</label>
                                            <input type="text" class="form-control" aria-describedby="nameHelp" name="instructor_notes" value="{{isset($courseData['instructor_notes'])? $courseData['instructor_notes'] : ''}}">           
                                        </div> 
                                    <div class="form-group col-md-6">
                                        <label for="exampleSelect1">Status</label>
                                        <select class="form-control" name="status">
                                            <option value="1" <?php echo (isset($courseData['status']) && $courseData['status'] =='1')?'selected' : '' ?>>Active</option>
                                            <option value="2" <?php echo (isset($courseData['status']) && $courseData['status'] =='2')?'selected' : '' ?>>Inactive</option>
                                        </select>
                                    </div> 
                                    <div class="form-group col-md-6">
                                        <input type="hidden" name="data" value="course">
                                    </div>       
                                    <div class="kt-portlet__foot">
                                        <div class="kt-form__actions">
                                            <button type="submit" class="btn btn-primary" id="{{ isset($edit)?'update':'add'}}">Submit</button>
                                            <a type="button" href="{{route('course',['param'=>'list'])}}" class="btn btn-secondary">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>                                     
                </div>
                <!--end: Datatable -->
            </div>
        </div>
    </div>
@endsection
                    