@extends('layouts.grid')
@section('content')

        <div class="kt-portlet__body kt-portlet__body--fit">
            <!--begin: Datatable -->
            @if(session()->has('message'))
                {{ session()->get('message') }}
            @endif
            <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded" id="local_data" style="">
                <div style="display: block;background-color: #fff;padding: 25px;">
                    <a href="{{ route('course',['param'=>'create'])}}" type="button">Add course</a>
                <table class="kt-datatable__table table-bordered" style="display: block;padding: 10px; min-height: 330px;width:100%;" id="course_table">
                    <thead class="kt-datatable__head">
                        <tr class="kt-datatable__row" style="left: 0px;">
                            
                            <th data-field="OrderID" class="kt-datatable__cell kt-datatable__cell--sort"><span style="width: 113px;">Sr no.</span></th>
                            <th data-field="Country" class="kt-datatable__cell kt-datatable__cell--sort"><span style="width: 113px;">Program</span></th>
                            <th data-field="ShipDate" class="kt-datatable__cell kt-datatable__cell--sort"><span style="width: 113px;">Instructor</span></th>
                            <th data-field="Status" class="kt-datatable__cell kt-datatable__cell--sort"><span style="width: 113px;">Course name</span></th>
                            <th data-field="Status" class="kt-datatable__cell kt-datatable__cell--sort"><span style="width: 113px;">Duration</span></th>
                            <th data-field="OrderID" class="kt-datatable__cell kt-datatable__cell--sort"><span style="width: 113px;">Agency</span></th>
                            <th data-field="Status" class="kt-datatable__cell kt-datatable__cell--sort"><span style="width: 113px;">Status</span></th>
                            <th data-field="Actions" data-autohide-disabled="false" class="kt-datatable__cell kt-datatable__cell--sort"><span style="width: 110px;">Actions</span></th>
                        </tr>
                    </thead>
                    <tbody class="kt-datatable__body" style="">
                        @php $i = 1; @endphp
                        @if(isset($courseList))
                            @foreach($courseList as $course)
                                <tr class="kt-datatable__row">
                                    <td class="kt-datatable__cell"><span style="width: 113px;">{{ $i++ }}</span></td>
                                    <td class="kt-datatable__cell"><span style="width: 113px;">{{ $course['title'] }}</span></td>
                                    <td class="kt-datatable__cell"><span style="width: 113px;">{{ $course['first_name'] }} {{ $course['last_name'] }}</span></td>
                                    <td class="kt-datatable__cell"><span style="width: 113px;">{{ $course['name'] }}</span></td>
                                    <td class="kt-datatable__cell"><span style="width: 113px;">{{ $course['duration'] }}</span></td>
                                    <td class="kt-datatable__cell"><span style="width: 113px;">{{ $course['location'] }}</span></td>
                                    <?php
                                        $object = new \App\library\functions;
                                    ?>
									<td class="kt-datatable__cell"><span style="width: 113px;">{{ $object->getStatus($course['status']) }}</span></td>
									<td class="kt-datatable__cell">
									<div class="d-inline">
										<a class="" href="{{ route('course',['param'=>'edit','param2'=>$course['id']])}}"><i class="kt-menu__link-icon flaticon-edit "></i>&nbsp;</a>
                                        <a class="" onclick="Delete('<?php echo $course['id'] ?>')" href="javascript:void(0)"><i class="kt-menu__link-icon flaticon-delete"></i>&nbsp;</a>                            
                                    </div> 
                                    </td> 
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
                </div>
            </div>
@endsection