@extends('layouts.main')
@section('content')

<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

	<!-- begin:: Subheader -->
	<div class="kt-subheader   kt-grid__item" id="kt_subheader">
		<div class="kt-container  kt-container--fluid ">
			<div class="kt-subheader__main">
				<h3 class="kt-subheader__title">
				Admin Dashboard </h3>
				<span class="kt-subheader__separator kt-hidden"></span>
				<!-- <div class="kt-subheader__breadcrumbs">
					<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
					<span class="kt-subheader__breadcrumbs-separator"></span>
					<a href="" class="kt-subheader__breadcrumbs-link">
					</a> -->

					<!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
				<!-- </div> -->
			</div>

		</div>
	</div>

	<!-- end:: Subheader -->


	<!-- sidebar and header ends here -->

       <section class="dashboard-fileds" id="team">
    <div class="container">	
        <div class="row">
			<div class="col-md-4 col-offset-md-4">
				<h2>Import file</h2>
				<form name="" method="Post" action="" enctype="multipart/form-data">
					  @csrf		
					
					<div class="form-group">
						<label for="email">Upload File:</label>
						<input type="file" name="upload_file" class="form-control"  required>
					</div>			
					<div class="form-submit">
					<input type="submit" name="submit" class="btn btn-default" value="Submit">
					</div>
				</form>
			
			</div>
		</div>
	</div>
</section>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
				<!-- BEGIN: Subheader -->
				<div class="m-subheader ">
					<div class="d-flex align-items-center">
						<div class="page-header">
							<div class="container-fluid">
								<div class="pull-right">
									<!--button type="button" data-toggle="tooltip" class="btn btn-default hidden-md hidden-lg"><i class="fa fa-filter"></i>
									</button>
									<a type="button" class="btn btn-primary"  href="javascript:void(0)" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i>
									</a>
									<!--button type="submit" form="form-product" data-toggle="tooltip" title="" class="btn btn-default" data-original-title="Copy"><i class="fa fa-copy"></i>
									</button>
									<button type="button" form="form-product" data-toggle="modal" data-target="#myModal1" title="" class="btn btn-danger"><i class="fa fa-trash-o"></i>
									</button-->
                               
								</div>
								
								
							</div>
						
						<div class="container-fluid">
							<div class="row">
							<div class="col-md-12 col-sm-12">
									<div class="panel panel-default">
										
										<div class="panel-body">
											<form id="form-product">
												<div class="table-responsive">
												
													<table class="table table-bordered table-hover" id="table_excel">
														<thead>
															<tr>
																
																
																<td class="text-left"> <a href="#" class="asc">Date</a> 
																</td>
																<td class="text-left"> <a href="#" class="asc">Track</a> 
																</td>
																<td class="text-left"> <a href="#" class="asc">Type</a> 
																</td>
																<td class="text-left"> <a href="#" class="asc">Distance</a> 
																</td>
																<td class="text-left"> <a href="#" class="asc">Horse</a> 
																</td>
																<td class="text-left"> <a href="#" class="asc">Jockey</a> 
																</td>
																<td class="text-left"> <a href="#" class="asc">Trainer</a> 
																</td>
																<td class="text-left"> <a href="#" class="asc">SP Fav</a> 
																</td>
																<td class="text-left"> <a href="#" class="asc">Industry SP</a> 
																</td>
																<td class="text-left"> <a href="#" class="asc">Betfair SP</a> 
																</td>
																<td class="text-left"> <a href="#" class="asc">IP Min</a> 
																</td>
																<td class="text-left"> <a href="#" class="asc">IP Max</a> 
																</td>
																<td class="text-left"> <a href="#" class="asc">Pre Min</a> 
																</td>
																<td class="text-left"> <a href="#" class="asc">Pre Max</a> 
																</td>
																<td class="text-left"> <a href="#" class="asc">Pred Industry SP</a> 
																</td>
																<td class="text-left"> <a href="#" class="asc">Place</a> 
																</td>
																<td class="text-left"> <a href="#" class="asc"> Runners</a> 
																</td>
																
																<!--td class="text-left"> <a href="#">Image</a> 
																</td-->
																
															</tr>
														</thead>
														<tbody>
															
              
															
														
															
														</tbody>
													</table>
												</div>
											</form>
											<div class="row">
												<div class="col-sm-6 text-left">
													<ul class="pagination">
														<li class="active"><span>1</span>
														</li>
														<li><a href="#">2</a>
														</li>
														<li><a href="#">&gt;</a>
														</li>
														<li><a href="#">&gt;|</a>
														</li>
													</ul>
												</div>
												<div class="col-sm-6 text-right">Showing 1 to 20 of 21 (2 Pages)</div>
												<button onclick ="demo()";>gfgggdctdt</button>
											</div>
										</div>
									</div>
								</div>
								
								
							</div>
						</div>
					</div>
					<!-- END: Subheader -->
				</div>
			</div>
			<!--------end -UNDER-dashboard------------------->
		</div>
 <script type="text/javascript">
 
	 function demo(){
		 $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });
               $('#table_excel').DataTable({
               processing: true,
               serverSide: true,
               ajax: '{{ url('dashboard') }}',
               columns: [
						{ data: 'id', name: 'id' },
                        { data: 'date', name: 'date' },
                        { data: 'track', name: 'track' },
                        { data: 'type', name: 'type' },
                        { data: 'distance', name: 'distance' },
                        { data: 'horse', name: 'horse' },
                        { data: 'jockey', name: 'jockey' },
                        { data: 'trainer', name: 'trainer' },
                        { data: 'sp_fav', name: 'sp_fav'},
                        { data: 'industry_sp', name: 'industry_sp' },
                        { data: 'betfair_sp', name: 'betfair_sp' },
                        { data: 'ip_min', name: 'ip_min' },
                        { data: 'ip_max', name: 'ip_max' },
                        { data: 'pre_min', name: 'pre_min' },
                        { data: 'pre_max', name: 'pre_max' },
                        { data: 'pred_industry_sp', name: 'pred_industry_sp' },
                        { data: 'place', name: 'place' },
                        { data: 'runners', name: 'runners' },
                      
                      
                     ]
            });
	   }


</script>
		@endsection                    