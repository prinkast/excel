@extends('layouts.main')
@section('content')

                    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid mt-5">

                        <div class="kt-portlet kt-portlet--mobile profile-div">
                                <!-- PROFILE -->
                                <div class="kt-portlet__head  kt-portlet__head--noborder">
                                    <div class="kt-portlet__head-label">
                                        <h3 class="kt-portlet__head-title">
                                        </h3>
                                    </div>
                                    <div class="kt-portlet__head-toolbar">
                                        <a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown">
                                           <i class="flaticon-more-1"></i>
                                        </a>
                                      
                                    </div>
                                </div>
                                <div class="kt-portlet__body kt-portlet__body--fit-y">
                                    <!--begin::Widget -->
                                    <div class="kt-widget kt-widget--user-profile-1">
                                        <div class="kt-widget__head">
                                            <?php 
                                                if(isset($instructorData['image']) && $instructorData['image'] != ''){
                                                    $image = $instructorData['image'];
                                                }else{
                                                    $image = '300_25.jpg';
                                                }

                                            ?>
                                            <div class="kt-widget__media">
                                                <img src="/public/images/instructor/{{$image}}" alt="image">
                                            </div>
                                            <div class="kt-widget__content">
                                                <div class="kt-widget__section">
                                                    <a href="#" class="kt-widget__username">
                                                        {{ $instructorData['firstname']}} {{ $instructorData['lastname'] }}    
                                                        <i class="flaticon2-correct kt-font-success"></i>                       
                                                    </a>
                                                    <span class="kt-widget__subtitle">
                                                        Instructor               
                                                    </span>

                                                </div>

                                                <div class="kt-widget__action">
                                                    <a type="button" href="{{ route('instructor',['param'=>'edit','param2'=>$instructorData['id']])}}" class="btn btn-info btn-sm">Edit</a>&nbsp;
                                                    <button type="button" class="btn btn-success btn-sm">follow</button>                         
                                                </div>

                                                </div>                                              

                                            </div>
                                        </div>
                                        <div class="kt-widget__body">
                                            <div class="kt-widget__content">
                                                <div class="kt-widget__info">
                                                    <span class="kt-widget__label">Email:</span>
                                                    <a href="#" class="kt-widget__data">{{ $instructorData['email'] }}</a>
                                                </div>
                                                <div class="kt-widget__info">
                                                    <span class="kt-widget__label">Phone:</span>
                                                    <a href="#" class="kt-widget__data">{{ $instructorData['phone'] }}</a>
                                                </div>
                                                <div class="kt-widget__info">
                                                    <?php
                                                        $object = new \App\library\functions;
                                                    ?>
                                                    <span class="kt-widget__label">Status: {{ $object->getStatus($instructorData['status']) }}</span>
                                        </div>
                                    </div>
                                <!-- END -->
                            </div>
                        </div>
                    </div>
                    @endsection
                    