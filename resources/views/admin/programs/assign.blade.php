@extends('layouts.form')
@section('content')

    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid mt-5">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__body kt-portlet__body--fit">
                <!--begin: Datatable -->
                <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded" id="local_data" style="">
					<form class="kt-form" method="post" id="programs_assign">
						<div class="kt-portlet__body " >
							<div class="row">
                                <div class="col-md-7">
                                    <input type="hidden" name="category_id" value="{{isset($category['id'])? $category['id'] : ''}}">
                                    <input type="hidden" name="method" value="{{ isset($edit)?'update':'add'}}">
                                    <div class="row">
                                           <input type="hidden" name="program_id" value="{{ $programid}}">
                                        <div class="form-group col-md-6">
                                            <label>Select Instructor</label>
                                            <select class="form-control" name="instructor[]" id="select_instructor" multiple="multiple">
                                                @if(isset($instructorList))
                                                    @foreach ($instructorList as $instructor)
                                                        <option value="{{ $instructor['id'] }}" <?php echo ($instructor['program_id'] ==$programid)?'selected':''?>>{{ $instructor['first_name']}} {{ $instructor['last_name'] }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                           
                                    <div class="kt-portlet__foot">
                                        <div class="kt-form__actions">
                                            <button type="button" class="btn btn-primary" id="assign_btn">Assign</button>
                                            <button type="reset" class="btn btn-secondary">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
						</div>
					</form>                                    
                </div>
                <!--end: Datatable -->
            </div>
        </div>
    </div>

@endsection
                    

