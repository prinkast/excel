@extends('layouts.grid')
@section('content')

        <div class="kt-portlet__body kt-portlet__body--fit">
            <!--begin: Datatable -->
            @if(session()->has('message'))
                {{ session()->get('message') }}
            @endif
            <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded" id="local_data" style="">
                <div style="display: block;background-color: #fff;padding: 25px;">
                    <a href="{{ route('program',['param'=>'create'])}}">Add program</a>
                <table class="kt-datatable__table table-bordered" style="display: block;padding: 10px; min-height: 330px;width:100%;" id="program_table">
                    <thead class="kt-datatable__head">
                        <tr class="kt-datatable__row" style="left: 0px;">
                            
                            <th data-field="OrderID" class="kt-datatable__cell kt-datatable__cell--sort"><span style="width: 113px;">Sr no.</span></th>
                            <th data-field="Country" class="kt-datatable__cell kt-datatable__cell--sort"><span style="width: 113px;">Title</span></th>
                            <th data-field="ShipDate" class="kt-datatable__cell kt-datatable__cell--sort"><span style="width: 113px;">Cost</span></th>
                            <th data-field="CompanyName" class="kt-datatable__cell kt-datatable__cell--sort"><span style="width: 113px;">Length</span></th>
                            <th data-field="CompanyName" class="kt-datatable__cell kt-datatable__cell--sort"><span style="width: 113px;">Validity</span></th>
                            <th data-field="ShipDate" class="kt-datatable__cell kt-datatable__cell--sort"><span style="width: 113px;">Pass percent</span></th>
                            <th data-field="CompanyName" class="kt-datatable__cell kt-datatable__cell--sort"><span style="width: 113px;">Need permit</span></th>
                            <th data-field="Status" class="kt-datatable__cell kt-datatable__cell--sort"><span style="width: 113px;">Status</span></th>
                            <th data-field="Actions" data-autohide-disabled="false" class="kt-datatable__cell kt-datatable__cell--sort"><span style="width: 110px;">Actions</span></th>
                        </tr>
                    </thead>
                    <tbody class="kt-datatable__body" style="">
                        @php $i = 1; @endphp
                        @if(isset($programList))
                            @foreach($programList as $program)
                                <tr class="kt-datatable__row">
                                    <td class="kt-datatable__cell"><span style="width: 113px;">{{ $i++ }}</span></td>
                                    <td class="kt-datatable__cell"><span style="width: 113px;">{{ $program->title }}</span></td>
                                    <td class="kt-datatable__cell"><span style="width: 113px;">{{ $program->cost }}</span></td>
                                    <td class="kt-datatable__cell"><span style="width: 113px;">{{ $program->length }}</span></td>
                                    <td class="kt-datatable__cell"><span style="width: 113px;">{{ $program->write_up }}</span></td>
                                    <td class="kt-datatable__cell"><span style="width: 113px;">{{ $program->pass_percent }}</span></td>
                                    <td class="kt-datatable__cell"><span style="width: 113px;">{{ $program->need_permit }}</span></td>
                                    <?php
                                        $object = new \App\library\functions;
                                    ?>

									<td class="kt-datatable__cell"><span style="width: 113px;">{{ $object->getStatus($program->status) }}</span></td>
                                    <td class="kt-datatable__cell">
                                    <div class="d-inline">
                                        <a class="" href="{{ route('program',['param'=>'edit','param2'=>$program->id]) }}"><i class="kt-menu__link-icon flaticon-edit "></i>&nbsp;</a>
                                        <a class="" href="{{ route('program',['param'=>'assign','param2'=>$program->id]) }}"><i class="kt-menu__link-icon 
                                            flaticon-list "></i>&nbsp;</a>
                                        <a class="" href="{{ route('program',['param'=>'view','param2'=>$program->id]) }}"><i class="kt-menu__link-icon 
                                            flaticon-medical "></i>&nbsp;</a>
                                        <a class=""  onclick="Delete('{{ $program->id }}')"  href="javascript:void(0)"><i class="kt-menu__link-icon flaticon-delete"></i>&nbsp;</a>                           
                                    </div>  
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
                </div>
            </div>
@endsection