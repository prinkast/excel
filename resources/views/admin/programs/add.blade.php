@extends('layouts.form')
@section('content')


    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid mt-5">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__body kt-portlet__body--fit">
                <!--begin: Datatable -->
                <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded" id="local_data" style="">
                    <form class="kt-form" method="post" id="programs_form">
                        <div class="kt-portlet__body " >
                            <div class="row">
                                <div class="col-md-7">
                                    <input type="hidden" name="program_id" value="{{isset($programsList['id'])? $programsList['id'] : ''}}">
                                    <input type="hidden" name="method" value="{{ isset($edit)?'update':'add'}}">
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="exampleSelect1">Select Category</label>
                                            <select class="form-control" name="category">
                                                @if(isset($categoryList))
                                                    @foreach ($categoryList as $category)
                                                        <option value="{{ $category['id'] }}" <?php echo (isset($programsList['category_id']) && $programsList['category_id'] ==$category['id'])?'selected' : '' ?>>{{ $category['name']}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Program Name</label>
                                            <input type="text" class="form-control" aria-describedby="nameHelp" placeholder="Enter program Name" name="program_name" value="{{isset($programsList['title'])? $programsList['title'] : ''}}">            
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Cost</label>
                                            <input type="text" class="form-control" aria-describedby="nameHelp" name="cost"  value="{{isset($programsList['cost'])? $programsList['cost'] : ''}}">            
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Program length</label>
                                            <input type="text" class="form-control" aria-describedby="nameHelp" name="length"  value="{{isset($programsList['length'])? $programsList['length'] : ''}}">            
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Program validity</label>
                                            <input type="text" class="form-control" aria-describedby="nameHelp" name="validity" value="{{isset($programsList['validity'])? $programsList['validity'] : ''}}">            
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Write up</label>
                                            <input type="text" class="form-control" aria-describedby="nameHelp" name="writeup"value="{{isset($programsList['write_up'])? $programsList['write_up'] : ''}}">            
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Pass percent</label>
                                            <input type="text" class="form-control" aria-describedby="nameHelp" name="percent" value="{{isset($programsList['pass_percent'])? $programsList['pass_percent'] : ''}}">            
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="exampleSelect1">Need permit</label>
                                            <select class="form-control" name="permit">
                                                <option value="1" <?php echo (isset($programsList['need_permit']) && $programsList['need_permit'] =='1')?'selected' : '' ?>>Yes</option>
                                                <option value="2" <?php echo (isset($programsList['need_permit']) && $programsList['need_permit'] =='2')?'selected' : '' ?>>No</option>
                                            </select>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label for="exampleSelect1">Status</label>
                                            <select class="form-control" name="status">
                                                <option value="1" <?php echo (isset($programsList['status']) && $programsList['status'] =='1')?'selected' : '' ?>>Active</option>
                                                <option value="2" <?php echo (isset($programsList['status']) && $programsList['status'] =='2')?'selected' : '' ?>>Inactive</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Color</label>
                                            <input type="color" class="form-control" aria-describedby="nameHelp" name="color" value="{{isset($programsList['colour'])? $programsList['colour'] : '#ff0000'}}">            
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>General message</label>
                                            <textarea rows="5" cols="15" class="form-control" aria-describedby="nameHelp" name="message" value="{{isset($programsList['general_message'])? $programsList['general_message'] : ''}}"></textarea>           
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Required documents</label>
                                            <select class="form-control" name="documents[]" id="select_doc" multiple="multiple">
                                                @if(isset($documents))
                                                    @foreach ($documents as $document)
                                                        <option value="{{ $document['id'] }}" <?php echo (isset($document['document_id']) && $document['document_id'] == $document['id'] && $document['cat_pro_id'] == $programsList['id'])?'selected' : '' ?>>{{ $document['name']}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div> 
                                    </div>
                                    <input type="hidden" name="data" value="program">   
                                    <div class="kt-portlet__foot">
                                        <div class="kt-form__actions">
                                            <button type="submit" class="btn btn-primary" id="{{ isset($edit)?'update':'add'}}">Submit</button>
                                            <a type="button" href="{{route('program',['param'=>'list'])}}"class="btn btn-secondary">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>                                    
                </div>
                <!--end: Datatable -->
            </div>
        </div>
    </div>
@endsection
                    