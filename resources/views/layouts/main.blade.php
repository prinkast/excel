<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Dashboard</title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="/public/flaticon/flaticon.css" rel="stylesheet" type="text/css" />
    <link href="/public/flaticon2/flaticon.css" rel="stylesheet" type="text/css" />
    <link href="/public/css/all.min.css" rel="stylesheet" type="text/css" />
    <link href="/public/css/style.bundle.css" rel="stylesheet" type="text/css" />
    <link href="/public/css/custom_style.css" rel="stylesheet" type="text/css" />
    <link href="/public/css/datatables.bundle.css" rel="stylesheet" type="text/css" />
    <link href="/public/css/custom_style.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="public/images/favicon.ico" />
    <link href="/public/css/{{Request :: segment(1)}}.css" type="text/css">
	<link  href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--fixed kt-page--loading">
    @if(Auth::user()->role == '1')
        @include('inc.admin.header')
    @else
        @include('inc.header')
    @endif
    @yield('content')
    @include('inc.admin.footer')
</body>



<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
</html>