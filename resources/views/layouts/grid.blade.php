<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" >
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
	<meta charset="utf-8" />
	<title>Dashboard</title>
	<meta name="description" content="Pagination options datatables examples"> 
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link href="/public/flaticon/flaticon.css" rel="stylesheet" type="text/css" />
    <link href="/public/flaticon2/flaticon.css" rel="stylesheet" type="text/css" />
    <link href="/public/css/all.min.css" rel="stylesheet" type="text/css" />
    <link href="/public/css/style.bundle.css" rel="stylesheet" type="text/css" />
    <link href="/public/css/datatables.bundle.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="public/images/favicon.ico" />

    <link href="/public/css/{{Request :: segment(1)}}.css" type="text/css">

</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--fixed kt-page--loading">
    @include('inc.admin.header')
    @include('inc.admin.sub_header')
    @yield('content')
    @include('inc.admin.footer')
</body>
<script src="/public/js/jquery.min.js" type="text/javascript"></script>
<script src="/public/js/popper.js" type="text/javascript"></script>
<script src="/public/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/public/js/js.cookie.js" type="text/javascript"></script>
<script src="/public/js/moment.min.js" type="text/javascript"></script>
<script src="/public/js/perfect-scrollbar.js" type="text/javascript"></script>
<script src="/public/js/sticky.js" type="text/javascript"></script>
<script src="/public/js/scripts.bundle.js" type="text/javascript"></script>
<script type="text/javascript" src="/public/js/datatables.bundle.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script src="/public/js/{{Request :: segment(1)}}.js" type="text/javascript"></script>
</html>
