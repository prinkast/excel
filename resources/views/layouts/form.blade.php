<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" >
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
        <meta charset="utf-8" />
        <title>Dashboard</title>
        <meta name="description" content="Pagination options datatables examples"> 
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<link href="/public/flaticon/flaticon.css" rel="stylesheet" type="text/css" />
	    <link href="/public/flaticon2/flaticon.css" rel="stylesheet" type="text/css" />
	    <link href="/public/css/all.min.css" rel="stylesheet" type="text/css" />
	    <link href="/public/css/style.bundle.css" rel="stylesheet" type="text/css" />
	    <link href="/public/css/datatables.bundle.css" rel="stylesheet" type="text/css" />
	    <link rel="shortcut icon" href="public/images/favicon.ico" />	
	    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

	    <link rel="stylesheet" type="text/css" href="/public/css/{{Request :: segment(1)}}.css">
	    <!-- Latest compiled and minified CSS -->
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />

</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--fixed kt-page--loading">
	@if(Auth::user()->role == '1')
    	@include('inc.admin.header')
    @else
    	@include('inc.header')
    @endif
    @include('inc.admin.sub_header')
    @yield('content')
    @include('inc.admin.footer')


</body>
<script src="/public/js/jquery.min.js" type="text/javascript"></script>
<script src="/public/js/popper.js" type="text/javascript"></script>
<script src="/public/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/public/js/js.cookie.js" type="text/javascript"></script>
<script src="/public/js/moment.min.js" type="text/javascript"></script>
<script src="/public/js/perfect-scrollbar.js" type="text/javascript"></script>
<script src="/public/js/sticky.js" type="text/javascript"></script>
<script src="/public/js/scripts.bundle.js" type="text/javascript"></script>
<script type="text/javascript" src="/public/js/jquery.validate.js"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<script src="/public/js/{{Request :: segment(1)}}.js" type="text/javascript"></script>
</html>




