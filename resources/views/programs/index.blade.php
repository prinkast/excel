@extends('layouts.main')
@section('content')

<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid mt-5">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
               
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                
                    <div class="dropdown dropdown-inline">
                        <a type="button" href="{{ route('program/create') }}" class="btn btn-brand btn-icon-sm">
                            <i class="flaticon2-plus"></i> Add New Program
                        </a>                        
                    </div>
                </div>
            </div>
        </div>
        </div>
        <div class="kt-portlet__body kt-portlet__body--fit">
            <!--begin: Datatable -->
            @if(session()->has('message'))
                {{ session()->get('message') }}
            @endif
            <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded" id="local_data" style="">
                <div style="display: block;background-color: #fff;padding: 25px;">
                <table class="kt-datatable__table table-bordered" style="display: block;padding: 10px; min-height: 330px;width:100%;" id="instructor_table">
                    <thead class="kt-datatable__head">
                        <tr class="kt-datatable__row" style="left: 0px;">
                            
                            <th data-field="OrderID" class="kt-datatable__cell kt-datatable__cell--sort"><span style="width: 113px;">Sr no.</span></th>
                            <th data-field="Country" class="kt-datatable__cell kt-datatable__cell--sort"><span style="width: 113px;">Title</span></th>
                            <th data-field="ShipDate" class="kt-datatable__cell kt-datatable__cell--sort"><span style="width: 113px;">Cost</span></th>
                            <th data-field="CompanyName" class="kt-datatable__cell kt-datatable__cell--sort"><span style="width: 113px;">Length</span></th>
                            <th data-field="CompanyName" class="kt-datatable__cell kt-datatable__cell--sort"><span style="width: 113px;">Validity</span></th>
                            <th data-field="Status" class="kt-datatable__cell kt-datatable__cell--sort"><span style="width: 113px;">Write Up</span></th>
                            <th data-field="ShipDate" class="kt-datatable__cell kt-datatable__cell--sort"><span style="width: 113px;">Pass percent</span></th>
                            <th data-field="CompanyName" class="kt-datatable__cell kt-datatable__cell--sort"><span style="width: 113px;">Need permit</span></th>
                            <th data-field="CompanyName" class="kt-datatable__cell kt-datatable__cell--sort"><span style="width: 113px;">General message</span></th>
                            <th data-field="Status" class="kt-datatable__cell kt-datatable__cell--sort"><span style="width: 113px;">Colour</span></th>
                            <th data-field="Status" class="kt-datatable__cell kt-datatable__cell--sort"><span style="width: 113px;">Status</span></th>
                            <th data-field="Actions" data-autohide-disabled="false" class="kt-datatable__cell kt-datatable__cell--sort"><span style="width: 110px;">Actions</span></th>
                        </tr>
                    </thead>
                    <tbody class="kt-datatable__body" style="">
                    	@php $i = 1; @endphp
                    	@if(isset($programList))
							@foreach($programList as $program)
		                        <tr class="kt-datatable__row">
									<td class="kt-datatable__cell"><span style="width: 113px;">{{ $i++ }}</span></td>
									<td class="kt-datatable__cell"><span style="width: 113px;">{{ $program->title }}</span></td>
									<td class="kt-datatable__cell"><span style="width: 113px;">{{ $program->cost }}</span></td>
									<td class="kt-datatable__cell"><span style="width: 113px;">{{ $program->length }}</span></td>
									<td class="kt-datatable__cell"><span style="width: 113px;">{{ $program->validity }}</span></td>
                                    <td class="kt-datatable__cell"><span style="width: 113px;">{{ $program->write_up }}</span></td>
                                    <td class="kt-datatable__cell"><span style="width: 113px;">{{ $program->pass_percent }}</span></td>
                                    <td class="kt-datatable__cell"><span style="width: 113px;">{{ $program->need_permit }}</span></td>
                                    <td class="kt-datatable__cell"><span style="width: 113px;">{{ $program->general_message }}</span></td>
                                    <td class="kt-datatable__cell"><span style="width: 113px;">{{ $program->colour }}</span></td>
                                    <?php
                                        $object = new \App\library\functions;
                                    ?>
									<td class="kt-datatable__cell"><span style="width: 113px;">{{ $object->getStatus($program->status) }}</span></td>
									<td class="kt-datatable__cell">
									<div class="d-inline">
										<a class="" href="{{ route('instructor', ['param'=>'edit','param2'=>$program->id]) }}"><i class="kt-menu__link-icon flaticon-edit "></i>&nbsp;</a>
                                        <a class="" href="{{ route('instructor', ['param'=>'edit','param2'=>$program->id]) }}"><i class="kt-menu__link-icon 
                                            flaticon-medical "></i>&nbsp;</a>
										<a class="" href="{{ route('instructor', ['param'=>'delete','param2'=>$program->id]) }}"><i class="kt-menu__link-icon flaticon-delete"></i>&nbsp;</a>                           
									</div>  
								</tr>
							@endforeach
						@endif
                    </tbody>
                </table>
                </div>
            </div>
@include('inc.footer')
<script type="text/javascript" src="/public/instructor/instructor.js"></script>
<script type="text/javascript" src="/public/js/datatables.bundle.js"></script>
@endsection