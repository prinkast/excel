@extends('layouts.main')
@section('content')
<link rel="stylesheet" type="text/css" href="/public/instructor/css/style.css">
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

<!-- begin:: Subheader -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                Dashboard </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
            </div>
        </div>
    </div>
    <!-- end:: Subheader -->
    <!-- sidebar and header ends here -->

    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid mt-5">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__body kt-portlet__body--fit">
                <!--begin: Datatable -->
                <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded" id="local_data" style="">
					<form class="kt-form" method="post" id="programs_form">
						<div class="kt-portlet__body " >
							<div class="row">
                                <div class="col-md-7">
                                    <input type="hidden" name="category_id" value="{{isset($category['id'])? $category['id'] : ''}}">
                                    <input type="hidden" name="method" value="{{ isset($edit)?'update':'add'}}">
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="exampleSelect1">Select Category</label>
                                            <select class="form-control" name="category">
                                                @if(isset($categoryList))
                                                    @foreach ($categoryList as $category)
                                                        <option value="{{ $category['id'] }}">{{ $category['name']}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
										<div class="form-group col-md-6">
											<label>Program Name</label>
											<input type="text" class="form-control" aria-describedby="nameHelp" placeholder="Enter program Name" name="program_name" value="">            
										</div>
                                        <div class="form-group col-md-6">
                                            <label>Cost</label>
                                            <input type="text" class="form-control" aria-describedby="nameHelp" name="cost"  value="">            
                                        </div>
										<div class="form-group col-md-6">
                                            <label>Program length</label>
                                            <input type="text" class="form-control" aria-describedby="nameHelp" name="length"  value="">            
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Program validity</label>
                                            <input type="text" class="form-control" aria-describedby="nameHelp" name="validity" value="">            
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Write up</label>
                                            <input type="text" class="form-control" aria-describedby="nameHelp" name="writeup"value="">            
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Pass percent</label>
                                            <input type="text" class="form-control" aria-describedby="nameHelp" name="percent" value="">            
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="exampleSelect1">Need permit</label>
                                            <select class="form-control" name="permit">
                                                <option value="1">Yes</option>
                                                <option value="2">No</option>
                                            </select>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label for="exampleSelect1">Status</label>
                                            <select class="form-control" name="status">
                                                <option value="1" >Active</option>
                                                <option value="2" >Inactive</option>
                                                <option value="3" >Deleted</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Color</label>
                                            <input type="text" class="form-control" aria-describedby="nameHelp" name="color" value="">            
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>General message</label>
                                            <textarea rows="5" cols="15" class="form-control" aria-describedby="nameHelp" name="message" value=""></textarea>           
                                        </div> 
                                    </div>
                                           
                                    <div class="kt-portlet__foot">
                                        <div class="kt-form__actions">
                                            <button type="submit" class="btn btn-primary" id="{{ isset($edit)?'update':'add'}}">Submit</button>
                                            <button type="reset" class="btn btn-secondary">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
						</div>
					</form>                                    
                </div>
                <!--end: Datatable -->
            </div>
        </div>
    </div>
@include('inc.footer')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="/public/programs/js/jquery.validate.js"></script>
<script type="text/javascript" src="/public/programs/js/programs.js"></script>

@endsection
                    