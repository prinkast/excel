@extends('layouts.form')
@section('content')

                    <!-- sidebar and header ends here -->

                    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid mt-5" >

                        <div class="kt-portlet kt-portlet--mobile">
                            <div class="kt-portlet__body kt-portlet__body--fit">
                                <!--begin: Datatable -->
                                <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded" id="local_data" style="">

                                    <form class="kt-form" method="post" action=" " id="student_form" enctype="multipart/form-data">
                                        <div class="kt-portlet__body " >
                                            <div class="row">

                                                <div class="col-md-3" style="display: none;">
                                                    <div class="main-div">
                                                        <div class="input_feild">
                                                        <input type="file" name="avatar" class="my-form" id="default_file"  onchange="Showimage(this);"/>
                                                        <div class="onclick_image">
                                                            <?php 
                                                            if(isset($studentData['photo']) && $studentData['photo'] != ''){
                                                                $image = $studentData['image'];
                                                            }else{
                                                                $image = '300_25.jpg';
                                                            }

                                                            ?>
                                                            <img src="/public/images/student/{{$image}}" class="my_profile_image" id="my_profile_image">
                                                        </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="hidden" name="student_id" value="{{isset($studentData['id'])? $studentData['id'] : ''}}">
                                                    <input type="hidden" name="method" value="{{ isset($studentData['edit'])?'update':''}}">
                                                    <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <label for="exampleSelect1">Select Category</label>
                                                        <select class="form-control" name="category">
                                                            @if(isset($categoryList))
                                                                @foreach ($categoryList as $category)
                                                                    <option value="{{ $category['id'] }}" <?php echo (isset($studentData['category_id']) && $studentData['category_id'] =='1')?'selected' : '' ?>>{{ $category['name']}}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label>First Name</label>
                                                        <input type="text" class="form-control" aria-describedby="nameHelp" placeholder="Enter First Name" name="firstname" value = "{{isset($studentData['first_name'])? $studentData['first_name'] : ''}}">            
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label>Last Name</label>
                                                        <input type="text" class="form-control" aria-describedby="nameHelp" placeholder="Enter Name" name="lastname" value="{{isset($studentData['last_name'])? $studentData['last_name'] : ''}}">            
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label>Email address</label>
                                                        <input type="email" class="form-control" aria-describedby="emailHelp" placeholder="Enter email" name="email" value="{{isset($studentData['email'])? $studentData['email'] : ''}}">            
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label>Phone</label>
                                                        <input type="text" class="form-control" placeholder="Enter Phone number" name="phone" value="{{isset($studentData['phone'])? $studentData['phone'] : ''}}">            
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label>Address</label>
                                                        <input type="text" class="form-control" placeholder="Enter Address" name="address" value="{{isset($studentData['address'])? $studentData['address'] : ''}}">            
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label>City</label>
                                                        <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="Enter city" name="city" value="{{isset($studentData['city'])? $studentData['city'] : ''}}">            
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label>Postal Code</label>
                                                        <input type="text" class="form-control" placeholder="Enter Phone number" name="postal_code" value="{{isset($studentData['postal_code'])? $studentData['postal_code'] : ''}}">            
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label>Birth city</label>
                                                        <input type="text" class="form-control" placeholder="Enter Phone number" name="birth_city" value="{{isset($studentData['birth_city'])? $studentData['birth_city'] : ''}}">            
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="exampleSelect1">Status</label>
                                                        <select class="form-control" name="status">
                                                            <option value="1" <?php echo (isset($studentData['status']) && $studentData['status'] =='1')?'selected' : '' ?>>Active</option>
                                                            <option value="2" <?php echo (isset($studentData['status']) && $studentData['status'] =='2')?'selected' : '' ?>>Inactive</option>
                                                        </select>
                                                    </div>
                                                   
                                                </div>
                                                    <input type="hidden" name="data" value="student">
                                                    <div class="kt-portlet__foot">
                                                        <div class="kt-form__actions">
                                                            <button type="submit" class="btn btn-primary" id="add">Submit</button>
                                                            <a type="button" href="{{route('instructor',['param'=>'list'])}}" class="btn btn-secondary">Cancel</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            
                                        </div>
                                        </div>
                                    </form>                                    
                                </div>
                                <!--end: Datatable -->
                            </div>
                        </div>
                    </div>
                    @endsection
                    