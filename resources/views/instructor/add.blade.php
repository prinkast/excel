@extends('layouts.main')
@section('content')

<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

    <!-- begin:: Subheader -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
						Dashboard </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link">
							Add New User </a>

                    <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                </div>
            </div>
        
        </div>
    </div>

    <!-- end:: Subheader -->


    <!-- sidebar and header ends here -->

    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid mt-5">

        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
				<i class="kt-font-brand flaticon2-line-chart"></i>
			</span>
                                    <h3 class="kt-portlet__head-title">
				Local Datasource
			</h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <a href="#" class="btn btn-clean btn-icon-sm">
                    <i class="la la-long-arrow-left"></i> Back
                </a>
                &nbsp;
                <div class="dropdown dropdown-inline">
                    <button type="button" class="btn btn-brand btn-icon-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="flaticon2-plus"></i> Add New
                    </button>
                    <div class="dropdown-menu dropdown-menu-right">
                        <ul class="kt-nav">
                            <li class="kt-nav__section kt-nav__section--first">
                                <span class="kt-nav__section-text">Choose an action:</span>
                            </li>
                            <li class="kt-nav__item">
                                <a href="#" class="kt-nav__link">
                                    <i class="kt-nav__link-icon flaticon2-open-text-book"></i>
                                    <span class="kt-nav__link-text">Reservations</span>
                                </a>
                            </li>
                            <li class="kt-nav__item">
                                <a href="#" class="kt-nav__link">
                                    <i class="kt-nav__link-icon flaticon2-calendar-4"></i>
                                    <span class="kt-nav__link-text">Appointments</span>
                                </a>
                            </li>
                            <li class="kt-nav__item">
                                <a href="#" class="kt-nav__link">
                                    <i class="kt-nav__link-icon flaticon2-bell-alarm-symbol"></i>
                                    <span class="kt-nav__link-text">Reminders</span>
                                </a>
                            </li>
                            <li class="kt-nav__item">
                                <a href="#" class="kt-nav__link">
                                    <i class="kt-nav__link-icon flaticon2-contract"></i>
                                    <span class="kt-nav__link-text">Announcements</span>
                                </a>
                            </li>
                            <li class="kt-nav__item">
                                <a href="#" class="kt-nav__link">
                                    <i class="kt-nav__link-icon flaticon2-shopping-cart-1"></i>
                                    <span class="kt-nav__link-text">Orders</span>
                                </a>
                            </li>
                            <li class="kt-nav__separator kt-nav__separator--fit">
                            </li>
                            <li class="kt-nav__item">
                                <a href="#" class="kt-nav__link">
                                    <i class="kt-nav__link-icon flaticon2-rocket-1"></i>
                                    <span class="kt-nav__link-text">Projects</span>
                                </a>
                            </li>
                            <li class="kt-nav__item">
                                <a href="#" class="kt-nav__link">
                                    <i class="kt-nav__link-icon flaticon2-chat-1"></i>
                                    <span class="kt-nav__link-text">User Feedbacks</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body kt-portlet__body--fit">
        <!--begin: Datatable -->
        <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded" id="local_data" style="">
			<form class="kt-form" method="post" action=" " id="add_user">
				<div class="kt-portlet__body " >
					<div class="row">
                        <input type="hidden" name="instructor_id" value="{{isset($instructorData['id'])? $instructorData['id'] : ''}}">
                        <input type="hidden" name="method" value="{{ isset($instructorData['edit'])?'update':''}}">
						<div class="form-group col-md-4">
							<label>First Name</label>
							<input type="text" class="form-control" aria-describedby="nameHelp" placeholder="Enter First Name" name="firstname" value="{{isset($instructorData['firstname'])? $instructorData['firstname'] : ''}}">            
						</div>
                        <div class="form-group col-md-4">
                            <label>Last Name</label>
                            <input type="text" class="form-control" aria-describedby="nameHelp" placeholder="Enter Name" name="lastname" value="{{isset($instructorData['lastname'])? $instructorData['lastname'] : ''}}">            
                        </div>
						<div class="form-group col-md-4">
							<label>Email address</label>
							<input type="email" class="form-control" aria-describedby="emailHelp" placeholder="Enter email" name="email" value="{{isset($instructorData['email'])? $instructorData['email'] : ''}}">            
						</div>
                        <div class="form-group col-md-4">
                            <label>Phone</label>
                            <input type="text" class="form-control" placeholder="Enter Phone number" name="phone" value="{{isset($instructorData['phone'])? $instructorData['phone'] : ''}}">            
                        </div>

                        <div class="form-group col-md-4">
                            <label for="exampleSelect1">Status</label>
                            <select class="form-control" name="status">
                                <option value="1"  selected="{{(isset($instructorData['status']) && $instructorData['status'] ==1)?'selected' : ''}}">Active</option>
                                <option value="2"selected="{{(isset($instructorData['status']) && $instructorData['status'] ==2)?'selected' : ''}}">Inactive</option>
                                <option value="3"selected="{{(isset($instructorData['status']) && $instructorData['status'] ==3)?'selected' : ''}}">Deleted</option>
                            </select>
                        </div>
						<div class="form-group col-md-12">
							<label for="exampleSelect2">Upload Image</label>
							<input type="file" class="form-control" name="upload" placeholder="Upload Image"/>
						</div>
					</div>
				</div>
				<div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<button type="button" class="btn btn-primary" id="add">Submit</button>
						<button type="reset" class="btn btn-secondary">Cancel</button>
					</div>
				</div>
			</form>                                    
        </div>
        <!--end: Datatable -->
    </div>
</div>
</div>

@include('inc.footer')
<script type="text/javascript" src="/public/instructor/instructor.js"></script>
@endsection                    