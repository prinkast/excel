@extends('layouts.main')
@section('content')
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid mt-5">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
               
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                
                    <div class="dropdown dropdown-inline">
                        <a type="button" href="{{ route('instructor', ['param'=>'create']) }}" class="btn btn-brand btn-icon-sm">
                            <i class="flaticon2-plus"></i> Add New User
                        </a>
                        
                    </div>
                </div>
            </div>
        </div>
        </div>
        <div class="kt-portlet__body kt-portlet__body--fit">
            <!--begin: Datatable -->
            @if(session()->has('message'))
                {{ session()->get('message') }}
            @endif
            <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded" id="local_data" style="">
                <div style="display: block;background-color: #fff;padding: 25px;">
                <table class="kt-datatable__table table-bordered" style="display: block;padding: 10px; min-height: 330px;width:100%;" id="instructor_table">
                    <thead class="kt-datatable__head">
                        <tr class="kt-datatable__row" style="left: 0px;">
                            
                            <th data-field="OrderID" class="kt-datatable__cell kt-datatable__cell--sort"><span style="width: 113px;">Sr no.</span></th>
                            <th data-field="Country" class="kt-datatable__cell kt-datatable__cell--sort"><span style="width: 113px;">First name</span></th>
                            <th data-field="ShipDate" class="kt-datatable__cell kt-datatable__cell--sort"><span style="width: 113px;">Last name</span></th>
                            <th data-field="CompanyName" class="kt-datatable__cell kt-datatable__cell--sort"><span style="width: 113px;">Email</span></th>
                            <th data-field="CompanyName" class="kt-datatable__cell kt-datatable__cell--sort"><span style="width: 113px;">Phone</span></th>
                            <th data-field="Status" class="kt-datatable__cell kt-datatable__cell--sort"><span style="width: 113px;">Status</span></th>
                            <th data-field="Actions" data-autohide-disabled="false" class="kt-datatable__cell kt-datatable__cell--sort"><span style="width: 110px;">Actions</span></th>
                        </tr>
                    </thead>
                    <tbody class="kt-datatable__body" style="">
                    	@php $i = 1; @endphp
                    	@if(isset($instructorList))
							@foreach($instructorList as $instructor)
		                        <tr class="kt-datatable__row">
									<td class="kt-datatable__cell"><span style="width: 113px;">{{ $i++ }}</span></td>
									<td class="kt-datatable__cell"><span style="width: 113px;">{{ $instructor->first_name }}</span></td>
									<td class="kt-datatable__cell"><span style="width: 113px;">{{ $instructor->last_name }}</span></td>
									<td class="kt-datatable__cell"><span style="width: 113px;">{{ $instructor->email }}</span></td>
									<td class="kt-datatable__cell"><span style="width: 113px;">{{ $instructor->phone }}</span></td>
                                    <?php
                                        $object = new \App\library\functions;
                                    ?>
									<td class="kt-datatable__cell"><span style="width: 113px;">{{ $object->getStatus($instructor->status) }}</span></td>
									<td class="kt-datatable__cell">
									<div class="d-inline">
										<a class="" href="{{ route('instructor', ['param'=>'edit','param2'=>$instructor->id]) }}"><i class="kt-menu__link-icon flaticon-edit "></i>&nbsp;</a>
                                        <a class="" href="{{ route('instructor', ['param'=>'edit','param2'=>$instructor->id]) }}"><i class="kt-menu__link-icon 
                                            flaticon-medical "></i>&nbsp;</a>
										<a class="" href="{{ route('instructor', ['param'=>'delete','param2'=>$instructor->id]) }}"><i class="kt-menu__link-icon flaticon-delete"></i>&nbsp;</a>                           
									</div>  
								</tr>
							@endforeach
						@endif
                    </tbody>
                </table>
                </div>
            </div>
@include('inc.footer')
<script type="text/javascript" src="/public/instructor/instructor.js"></script>
<script type="text/javascript" src="/public/js/datatables.bundle.js"></script>
@endsection