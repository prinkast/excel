<!-- begin:: Aside Menu sidebar -->
    <div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
        <div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1">

            <ul class="kt-menu__nav ">
                <li class="kt-menu__item  kt-menu__item--submenu  {{ (Request::segment(1) == 'instructor')?'active':''}}" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="{{ route('instructor',['param'=>'list']) }}" class="text-white kt-menu__link kt-menu__toggle text-white"><i class="kt-menu__link-icon flaticon-users text-white "></i><span class="kt-menu__link-text text-white">upload File</span><i class="kt-menu__ver-arrow fa fa-angle-right" style="display:none;"></i></a>
                </li>
                <!--li class="kt-menu__item  kt-menu__item--submenu   {{ (Request::segment(1) == 'category')?'active':''}}" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="{{ route('category',['param'=>'list']) }}" class="text-white kt-menu__link kt-menu__toggle text-white"><i class="kt-menu__link-icon flaticon2-cube text-white "></i><span class="kt-menu__link-text text-white">Manage Categories</span><i class="kt-menu__ver-arrow fa fa-angle-right"  style="display:none;"></i></a>
                </li>
                <li class="kt-menu__item  kt-menu__item--submenu  {{ (Request::segment(1) == 'instructor')?'program':''}}" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="{{ route('program',['param'=>'list']) }}" class="text-white kt-menu__link kt-menu__toggle text-white"><i class="kt-menu__link-icon flaticon2-list-2 text-white "></i><span class="kt-menu__link-text  text-white">Manage Programs</span><i class="kt-menu__ver-arrow fa fa-angle-right" style="display:none;"></i></a>
                </li>
                <li class="kt-menu__item  kt-menu__item--submenu  {{ (Request::segment(1) == 'course')?'active':''}}"  aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="{{ route('course',['param'=>'list']) }}" class="text-white kt-menu__link kt-menu__toggle text-white"><i class="kt-menu__link-icon flaticon2-list-2 text-white "></i><span class="kt-menu__link-text  text-white">Manage Courses</span><i class="kt-menu__ver-arrow fa fa-angle-right" style="display:none;"></i></a>
                </li>
                </li>
                <li class="kt-menu__item  kt-menu__item--submenu"  aria-haspopup="true" data-ktmenu-submenu-toggle="hover" style="display:none;"><a href="" class="text-white kt-menu__link kt-menu__toggle text-white"><i class="kt-menu__link-icon flaticon-interface-11 text-white "></i><span class="kt-menu__link-text text-white">Manage Documents</span><i class="kt-menu__ver-arrow fa fa-angle-right" style="display:none;"></i></a>
                </li>
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover" style="display:none;"><a href="" class="text-white kt-menu__link kt-menu__toggle text-white"><i class="kt-menu__link-icon flaticon2-list-3 text-white "></i><span class="kt-menu__link-text text-white">Manage Notice / Emails</span><i class="kt-menu__ver-arrow fa fa-angle-right"  style="display:none;"></i></a>
                </li>
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover" style="display:none;"><a href="" class="text-white kt-menu__link kt-menu__toggle text-white"><i class="kt-menu__link-icon flaticon-calendar-1 text-white "></i><span class="kt-menu__link-text text-white">Manage Calendar</span><i class="kt-menu__ver-arrow fa fa-angle-right"  style="display:none;"></i></a>
                </li>
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover" style="display:none;"><a href="" class="text-white kt-menu__link kt-menu__toggle text-white"><i class="kt-menu__link-icon flaticon-settings text-white "></i><span class="kt-menu__link-text text-white">Settings</span><i class="kt-menu__ver-arrow fa fa-angle-right" style="display:none;"></i></a></li-->
            </ul>
        </div>
    </div>
<!-- end:: Aside Menu -->
