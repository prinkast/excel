<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

    <!-- begin:: Subheader -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                 <h2 class="breadcrumb__title">
                <a href="{{ route(strtolower(@$title[0]),['param'=>'list'])}}">{{ @$title[0]}} </a></h2>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="{{ @$title[2] }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="javascript:void(0)" class="kt-subheader__breadcrumbs-link">
                    {{@$title[1]}} </a>

                </div>
            </div>
        
        </div>
    </div>