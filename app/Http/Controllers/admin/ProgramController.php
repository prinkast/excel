<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Program;
use App\User;
use App\library\functions;
class ProgramController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index($param,$param2=null){
        
        $object = new Functions;
        switch($param):
            case 'list' :
                $title = ['Program','List','program/list'];
                $program = Program::where('status','<>','3')->get();
                return view('admin.programs.index',['programList'=>$program,'title'=>$title]);
            break;
            case 'create' :
                $title = ['Program','Add','program/create'];
                $category_list = DB::table('category')
                                    ->where('status','<>','3')
                                    ->get();
                $documents = DB::table('instructor_docs')->get();                    
                return view('admin.programs.add',['categoryList'=>json_decode($category_list,true),'title'=>$title,'documents'=>json_decode($documents,true)]);
            break;
            case 'edit' :
                $title = ['Program','Edit','program/edit'];
                $category_list = DB::table('category')
                                    ->where('status','<>','3')
                                    ->get();
                $programs = Program::where('id', $param2)
                               ->first();
                $documents = DB::table('instructor_docs')
                    ->leftJoin('docs_for_program', 'docs_for_program.document_id', '=', 'instructor_docs.id')
                    ->select('instructor_docs.*', 'docs_for_program.document_id','docs_for_program.cat_pro_id')
                    ->get();
                return view('admin.programs.add',['programsList'=>json_decode($programs,true),'edit'=>'true','categoryList'=>json_decode($category_list,true),'title'=>$title,'documents'=>json_decode($documents,true)]);
            break;
            case 'delete':
                $object = new Functions;
                $programdata['status'] = '3';
                $response =  $object->updateRecord('program',$programdata,['id'=>$param2]);
                if($response){
                    $msg = [ 'status' => true,'message' => "Successfully deleted"];
                }else{
                    $msg = [ 'status' => true,'message' => "Oops something went wrong"];
                } 
                return response()->json($msg);
            break;
            case 'assign':
                $title = ['Program','Assign to instructor','program/assign'];
                $instructor = DB::table('users')
                                   ->leftJoin('instructor_program', 'instructor_program.instructor_id', '=', 'users.id')
                                   ->select('users.*', 'instructor_program.instructor_id', 'instructor_program.program_id')
                                   ->where('users.status','<>','3')
                                   ->get(); 
                return view('admin.programs.assign',['programid'=>$param2,'instructorList'=>json_decode($instructor,true),'title'=>$title]);
            break;
            case 'view':
                $title = ['Program','Assigned instructor','program/view'];
                $programData = DB::table('program')
                                   ->leftJoin('instructor_program', 'instructor_program.program_id', '=', 'program.id')
                                   ->leftJoin('users', 'users.id', '=', 'instructor_program.instructor_id')
                                   ->select('program.*', 'instructor_program.instructor_id', 'users.first_name','users.last_name')
                                   ->where([['program.id','=',$param2],['instructor_program.status','<>','3']])
                                   ->get(); 
                return view('admin.programs.view',['programData'=>$programData,'title'=>$title]);
            break;
            default :
                return view('admin.programs.index');
        endswitch;   
    }

    public function AssignProgram(Request $request,$param){
        $object = new Functions;
       switch($param):
            case 'save' :
                foreach ($request->instructor as $keys => $instructors) {
                    $data = ['program_id'=>$request->program_id,'instructor_id'=>$instructors];
                    DB::table('instructor_program')
                        ->updateOrInsert(
                            ['program_id'=>$request->program_id,'instructor_id'=>$instructors],
                            ['program_id'=>$request->program_id,'instructor_id'=>$instructors]
                        );
                }
                
                $msg = [ 'status' => true,'message' => "Programs assigned successfully"];
                
                return response()->json($msg);
            break;
            case 'delete':
                $data = ['status'=>'3'];
                $response = $object->updateRecord('instructor_program',$data,[['program_id','=',$request->program],['instructor_id','=',$request->instructorid]]);
                if($response){
                    $msg = [ 'status' => true,'message' => "Successfully deleted"];
                }else{
                    $msg = [ 'status' => false,'message' => "something went wrong"];  
                }
                return response()->json($msg);
            break;
        endswitch; 
    }

}
