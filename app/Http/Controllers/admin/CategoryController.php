<?php

namespace App\Http\Controllers\admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\library\functions;
use Auth;
use DB;
class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     *@return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($param,$param2=null){
        
        $object = new Functions;
        switch($param):
            case 'list' :
                $title = ['Category','List'];
                $category = Category::where('status','<>','3')->get();
                return view('admin.category.index',['categroyList'=>$category,'title'=>$title]);
            break;
            case 'create' :
                $title = ['Category','Create'];
                $documents = DB::table('instructor_docs')->get();
                return view('admin.category.add',['title'=>$title,'documents'=>json_decode($documents,true)]);
            break; 
            case 'edit' :
                $title = ['Category','Edit'];
                $category = Category::where('id', $param2)
                                    ->first();
                $documents = DB::table('instructor_docs')
                                ->leftJoin('docs_for_program', 'docs_for_program.document_id', '=', 'instructor_docs.id')
                                ->select('instructor_docs.*', 'docs_for_program.document_id','docs_for_program.cat_pro_id')
                                ->get();                    
                return view('admin.category.add',['category'=>$category,'edit'=>'true','title'=>$title,'documents'=>json_decode($documents,true)]);
            break;
            case 'delete':
                $categorydata['status'] = '3';
                $response =  $object->updateRecord('category',$categorydata,['id'=>$param2]);
                if($response){
                    $msg = [ 'status' => true,'message' => "Successfully deleted"];
                }else{
                    $msg = [ 'status' => true,'message' => "Oops something went wrong"];
                } 
                return response()->json($msg);
            break;
            default :
                $title = ['Category','List'];
                $category = Category::where('status','<>','3')->get();
                return view('admin.category.index',['categroyList'=>$category,'title'=>$title]);
        endswitch;   
    }

    
}
