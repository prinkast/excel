<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Course;
use App\library\functions;
class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index($param,$param2=null){
        
        $object = new Functions;
        switch($param):
            case 'list' :
                $title = ['Course','List'];
                $courseList = DB::table('courses')
                                   ->leftJoin('program', 'program.id', '=', 'courses.program_id')
                                   ->leftJoin('users', 'users.id', '=', 'courses.instructor_id')
                                   ->select('courses.*', 'program.title', 'users.first_name','users.last_name')
                                   ->where('courses.status','<>','3')
                                   ->get(); 
                return view('admin.course.index',['courseList'=>json_decode($courseList,true),'title'=>$title]);
            break;
            case 'create' :
                $title = ['Course','Create',];
                $programsList = DB::table('program')
                                ->where('status','<>','3')
                                ->get();
                $instructorList = DB::table('users')
                                ->where([['role', '=', '2'],['status', '<>', '3']])
                                ->get();
                return view('admin.course.create',['programList'=>json_decode($programsList,true),'instructorList'=>json_decode($instructorList,true),'title'=>$title]);
            break;
            case 'edit' :
                $title = ['Course','Edit'];
                $programsList = DB::table('program')
                                ->where('status','<>','3')
                                ->get();
                $instructorList = DB::table('users')
                                        ->where([['role', '=', '2'],['status', '<>', '3']])
                                        ->get();
                $Course = Course::where('id', $param2)
                               ->first();
                return view('admin.course.create',['programList'=>json_decode($programsList,true),'instructorList'=>json_decode($instructorList,true),'courseData'=>json_decode($Course,true),'edit'=>'true','title'=>$title]);
            break;
            case 'delete':
                $course['status'] = '3';
                $response =  $object->updateRecord('courses',$course,['id'=>$param2]);
                if($response){
                    $msg = [ 'status' => true,'message' => "Successfully deleted"];
                }else{
                    $msg = [ 'status' => true,'message' => "Oops something went wrong"];
                } 
                return response()->json($msg);
            break;
            default :
                return view('admin.course.index');
        endswitch;   
    }
   

}
