<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\UploadFile;
use Illuminate\Support\Facades\Hash;
use \App\library\functions;
use DB;
use File;
use Auth;
use Datatables;
use Illuminate\Support\Facades\Facade;
use Yajra\DataTables\Services\DataTable;
//use Excel;
use Importer;
use Maatwebsite\Excel\Facades\Excel;
class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function dashboard(Request $request){
		// var_dump($data);die;
		 if ($request->ajax()) {
		  
		  $data = UploadFile::latest()->get();
		 // var_dump($data);die;
		  
			//$data1 = Product_images::latest()->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
   
                           $btn = '<a href="javascript:void(0)"     data-id="'.$row->id.'" data-original-title="Edit" form="form-product" data-toggle="modal" data-target="#myModal"  class="edit btn btn-primary btn-sm editProduct">Edit</a>';
   $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteProduct">Delete</a>';
    
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
		
        return view('admin.users.dashboard');
    }
	public function upload_file(Request $request){
		
		//var_dump($_FILES);die;
		
		$upload_file  = $request->file('upload_file');
		//$user = Auth::user()->id;
		$path = $request->file('upload_file')->getRealPath();
		
		 $data = \Excel::load($path)->get();
		
		 $importfile = json_decode(json_encode($data), true);
		 
    	//if($data->count()){
			//$item=[];
			$arr=[];
			
                foreach ($importfile as $item) {
					 foreach ($item as $items) {
					
					$value=$items;
					$val=$value['date'];
					 // if($i>=0){
						
					 $arr[] = ['date' => $val['date'] ,'track' => $value['track'] , 'type' => $value['type'] , 'distance' => $value['distance'] , 'horse' => $value['horse'] , 'jockey' => $value['jockey'] , 'trainer' => $value['trainer'] , 'sp_fav' => $value['sp_fav'] , 'industry_sp' => $value['industry_sp'] , 'betfair_sp' => $value['betfair_sp'] , 'ip_min' => $value['ip_min'] , 'ip_max' => $value['ip_max'], 'pre_min' => $value['pre_min'], 'pre_max' => $value['pre_max'], 'pred_industry_sp' => $value['pred_industry_sp'], 'place' => $value['place'], 'runners' => $value['runners']];
               
					//var_dump($user);
                    //$arr[] = ['name' => $value->name, 'details' => $value->details];
					 }}
				 // }
				
				//var_dump($arr);die;
				
               if(!empty($arr)){
                    \DB::table('upload_file')->insert($arr);
                   // dd('Insert Record successfully.');
                } 
            //
		
        return view('admin.users.dashboard');
    }
	
	
	
	
}
