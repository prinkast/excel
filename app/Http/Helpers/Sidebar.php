<?php 
use App\Role_has_permissions;
use Illuminate\Http\Request;
function permissions($role,$segment1,$segment2){
			$permission = '';
			$rolePermissiom  = new Role_has_permissions();
			$permissions_detail	= $rolePermissiom->gerRolePermission($role);
			$perList = [];
			$permission_list = [];
			foreach($permissions_detail as $detail):
				$perList[$detail->name] = $detail->slug;
				$permission_list[] = $detail->name;
			endforeach; 
			
				if(in_array("home", $permission_list)):
				$activeClass = ($segment1=="home")?"m-menu__item--active":"" ;
				$permission .= '<li class="m-menu__item '.$activeClass.'" aria-haspopup="true" >
									<a  href="'. route('home') .'" class="m-menu__link ">
										<i class="m-menu__link-icon flaticon-line-graph"></i>
										<span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
										<span class="m-menu__link-text">Dashboard </span>
										</span></span>
									</a>
								</li>';
				endif;
				if(in_array("permission", $permission_list)):
				$activeClass = ($segment1=="permission")?"m-menu__item--open":"" ;
				$activeClassSubmenu = ($segment2=="assign")?"m-menu__item--active":"" ;
				$permission .= '<li class="m-menu__item  m-menu__item--submenu '.$activeClass.'" aria-haspopup="true"  m-menu-submenu-toggle="hover"><a  href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon la la-balance-scale"></i><span class="m-menu__link-text">Permission</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
									<div class="m-menu__submenu "><span class="m-menu__arrow"></span>
										<ul class="m-menu__subnav">
											<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" ><span class="m-menu__link"><span class="m-menu__link-text">'.trans('file.'.$perList['permission'].'').'</span></span></li>
											<li class="m-menu__item '.$activeClassSubmenu.'" aria-haspopup="true" ><a  href="'. route('permission', ['param'=>'assign']) .'" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Assign Permission</span></a></li>
										</ul>
									</div>
								</li>';
				endif;
				$permission .= '<li class="m-menu__section ">
									<h4 class="m-menu__section-text">Tools</h4>
									<i class="m-menu__section-icon flaticon-more-v2"></i>
								</li>';
								
				if($role =="admin"):
					
				$permission .= '<li class="m-menu__item" aria-haspopup="true" ><a  href="'. route("main_manager", ["param"=>"list"]) .'" class="m-menu__link "><i class="m-menu__link-icon fa fa-user"></i>
									<span class="m-menu__link-text">Main Managers</span></a>
								</li>
								<li class="m-menu__item" aria-haspopup="true" ><a  href="'. route("sub_manager", ["param"=>"list"]) .'" class="m-menu__link "><i class="m-menu__link-icon fa fa-user"></i>
									<span class="m-menu__link-text">Sub Managers</span></a>
								</li>';
				else:
				if(in_array("manager", $permission_list)):
			 
				$activeClass = ($segment1=="manager")?"m-menu__item--active":"" ;
				$permission .= '<li class="m-menu__item '.$activeClass.' " aria-haspopup="true" ><a  href="'. route("manager", ["param"=>"list"]) .'" class="m-menu__link "><i class="m-menu__link-icon fa fa-user"></i>
										<span class="m-menu__link-text">'.trans('file.'.$perList['manager'].'').'</span></a>
								</li>';
				endif;

				endif;
				
				if(in_array("user", $permission_list)):
				$activeClass = ($segment1=="user")?"m-menu__item--active":"" ;
				$permission .= '<li class="m-menu__item '.$activeClass.' " aria-haspopup="true" ><a  href="'. route("user", ["param"=>"list"]) .'" class="m-menu__link "><i class="m-menu__link-icon fa fa-user"></i>
									<span class="m-menu__link-text">'.trans('file.'.$perList['user'].'').'</span></a>
								</li>';
				endif;
				if(in_array("driver", $permission_list)):	
				$activeClass = (($segment1=="driver") || ($segment1=="box-driver" && $segment2=="list") || ($segment1=="currency" && $segment2=="driver"))?"m-menu__item--open":"" ;
				$activeClassSubmenu = ($segment2=="list" && $segment1=="driver")?"m-menu__item--active":"" ;
				$activeClassSubmenu1 = ($segment1=="box-driver" && $segment2=="list" )?"m-menu__item--active":"" ;
				$activeClassSubmenu2 = ($segment1=="currency" && $segment2=="driver")?"m-menu__item--active":"" ;
				$permission .= '<li class="m-menu__item  m-menu__item--submenu '.$activeClass.'" aria-haspopup="true"  m-menu-submenu-toggle="hover">
									<a  href="javascript:;" class="m-menu__link m-menu__toggle">
										<i class="m-menu__link-icon fa fa-bicycle"></i>
										<span class="m-menu__link-text">'.trans('file.'.$perList['driver'].'').'</span>
										<i class="m-menu__ver-arrow la la-angle-right"></i>
									</a>
								<div class="m-menu__submenu "><span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" ><span class="m-menu__link"><span class="m-menu__link-text">'.trans('file.'.$perList['driver'] .'').'</span></span></li>
										<li class="m-menu__item '.$activeClassSubmenu.'" aria-haspopup="true" ><a  href="'. route("driver", ["param"=>"list"]) .'" class="m-menu__link"><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
											<span class="m-menu__link-text">Driver List</span></a></li>
										<li class="m-menu__item '.$activeClassSubmenu1.'" aria-haspopup="true" ><a  href="'. route("box-driver", ["param"=>"list"]) .'" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
											<span class="m-menu__link-text">Box Driver</span></a></li>
										<li class="m-menu__item '.$activeClassSubmenu2.'" aria-haspopup="true" ><a  href="'. route("currency", ["param"=>"driver"]) .'" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
											<span class="m-menu__link-text">Driver Currency</span></a></li>
									</ul>
								</div></li>';
				endif;
				$permission .= '<li class="m-menu__section ">
									<h4 class="m-menu__section-text">Snippets</h4>
									<i class="m-menu__section-icon flaticon-more-v2"></i>
								</li>';
				if(in_array("warehouse", $permission_list)):
				$activeClass = ($segment1=="warehouse")?"m-menu__item--open":"" ;
				$activeClassSubmenu = ($segment1=="warehouse" && $segment2=="add")?"m-menu__item--active":"" ;
				$activeClassSubmenu1 = ($segment1=="warehouse" && $segment2=="list")?"m-menu__item--active":"" ;
				$permission .= '<li class="m-menu__item  m-menu__item--submenu '.$activeClass.'" aria-haspopup="true"  m-menu-submenu-toggle="hover"><a  href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon fa fa-warehouse"></i><span class="m-menu__link-text">'.trans('file.Warehouse').'</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
									<div class="m-menu__submenu "><span class="m-menu__arrow"></span>
										<ul class="m-menu__subnav">
											<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" ><span class="m-menu__link"><span class="m-menu__link-text">'.trans('file.Warehouse').'</span></span></li>
											<li class="m-menu__item '.$activeClassSubmenu.' " aria-haspopup="true" ><a  href="'.route('warehouse', ['param'=>'add']) .'" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Add Warehouse</span></a></li>
											<li class="m-menu__item '.$activeClassSubmenu1.'" aria-haspopup="true" ><a  href="'. route('warehouse', ['param'=>'list']) .'" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">View Warehouse</span></a></li>
										</ul>
									</div>
								</li>';
				endif;
				if(in_array("inventory", $permission_list)):
				$activeClass = ($segment1=="product-profile")?"m-menu__item--open":"" ;
				$activeClassSubmenu = ($segment1=="product-profile" && $segment2=="add")?"m-menu__item--active":"" ;
				$activeClassSubmenu1 = ($segment1=="product-profile" && $segment2=="list")?"m-menu__item--active":"" ;
				$permission .= '<li class="m-menu__item  m-menu__item--submenu '.$activeClass.'" aria-haspopup="true"  m-menu-submenu-toggle="hover"><a  href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-suitcase"></i><span class="m-menu__link-text">'.trans('file.'.$perList['inventory'] .'').'</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
									<div class="m-menu__submenu "><span class="m-menu__arrow"></span>
										<ul class="m-menu__subnav">
											<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" ><span class="m-menu__link"><span class="m-menu__link-text">'.trans('file.Inventory').'</span></span></li>
											<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover">
												<a  href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">'.trans('file.Raw product').'</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
												<div class="m-menu__submenu "><span class="m-menu__arrow"></span>
													<ul class="m-menu__subnav">
														<li class="m-menu__item " aria-haspopup="true" ><a href="'. route('raw-product', ['param'=>'add']) .'" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">'.trans('file.Add Raw Product').'</span></a></li>
														<li class="m-menu__item " aria-haspopup="true" ><a href="'. route('raw-product', ['param'=>'list']).'" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">'.trans('file.View Raw Product').'</span></a></li>
													</ul>
												</div>
											</li>
											<li class="m-menu__item  m-menu__item--submenu '.$activeClass.'" aria-haspopup="true"  m-menu-submenu-toggle="hover">
												<a  href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Product Profile</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
												<div class="m-menu__submenu "><span class="m-menu__arrow"></span>
													<ul class="m-menu__subnav">
														<li class="m-menu__item '.$activeClassSubmenu.'" aria-haspopup="true" ><a href="'.route('product-profile', ['param'=>'add']) .'" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Add Product Profile</span></a></li>
														<li class="m-menu__item '.$activeClassSubmenu1.'" aria-haspopup="true" ><a href="'.route('product-profile', ['param'=>'list']) .'" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">View Product Profile</span></a></li>
													</ul>
												</div>
											</li>
										</ul>
									</div>
								</li>';
				endif;
				if(in_array("store", $permission_list)):
				$activeClass = ($segment1=="store")?"m-menu__item--open":"" ;
				$activeClassSubmenu = ($segment1=="store" && $segment2=="add")?"m-menu__item--active":"" ;
				$activeClassSubmenu1 = ($segment1=="store" && $segment2=="list")?"m-menu__item--active":"" ;
				$permission .= '<li class="m-menu__item  m-menu__item--submenu '.$activeClass.'" aria-haspopup="true"  m-menu-submenu-toggle="hover"><a  href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon fa fa-store"></i><span class="m-menu__link-text">'.trans('file.Store').'</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
									<div class="m-menu__submenu "><span class="m-menu__arrow"></span>
										<ul class="m-menu__subnav">
											<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" ><span class="m-menu__link"><span class="m-menu__link-text">'.trans('file.Store').'</span></span></li>
											<li class="m-menu__item '.$activeClassSubmenu.' " aria-haspopup="true" ><a  href="'. route('store', ['param'=>'add']) .'" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Add Store</span></a></li>
											<li class="m-menu__item '.$activeClassSubmenu1.' " aria-haspopup="true" ><a  href="'. route('store', ['param'=>'list']) .'" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">View Store</span></a></li>
										</ul>
									</div>
								</li>';
				endif;
				if(in_array("box", $permission_list)):
				$activeClass = ($segment1=="box")?"m-menu__item--active":"" ;
				$permission .= '<li class="m-menu__item '.$activeClass.'" aria-haspopup="true" >
									<a href="'. route('box', ['param'=>'list']) .'" class="m-menu__link ">
										<i class="m-menu__link-icon fa fa-box-open"></i>
										<span class="m-menu__link-text">Boxes</span>
									</a>
								</li>';
				endif;
				if(in_array("currency", $permission_list)):
				$activeClass = ($segment1=="currency")?"m-menu__item--open":"" ;
				$activeClassSubmenu = ($segment1=="currency" && $segment2=="list")?"m-menu__item--active":"" ;
				$activeClassSubmenu1 = ($segment1=="currency" && $segment2=="assign-to-driver")?"m-menu__item--active":"" ;
				$permission .= '<li class="m-menu__item  m-menu__item--submenu '.$activeClass.'" aria-haspopup="true"  m-menu-submenu-toggle="hover"><a  href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon fa fa-money-bill-wave"></i><span class="m-menu__link-text">Currency</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
									<div class="m-menu__submenu "><span class="m-menu__arrow"></span>
										<ul class="m-menu__subnav">
											<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" ><span class="m-menu__link"><span class="m-menu__link-text">Currency</span></span></li>
											<li class="m-menu__item '.$activeClassSubmenu.'" aria-haspopup="true" ><a  href="'. route('currency', ['param'=>'list']) .'" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Currency List</span></a></li>
											<li class="m-menu__item '.$activeClassSubmenu1.'" aria-haspopup="true" ><a  href="'. route('currency', ['param'=>'assign-to-driver']) .'" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Assign Currency</span></a></li>
										</ul>
									</div>
								</li>';
				endif;
				if(in_array("group", $permission_list)):
				$activeClass = ($segment1=="group")?"m-menu__item--open":"" ;
				$activeClassSubmenu = ($segment1=="group" && $segment2=="list")?"m-menu__item--active":"" ;
				$permission .= '<li class="m-menu__item  m-menu__item--submenu '.$activeClass.'" aria-haspopup="true"  m-menu-submenu-toggle="hover"><a  href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon fa fa-user-friends"></i><span class="m-menu__link-text">Groups</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
									<div class="m-menu__submenu "><span class="m-menu__arrow"></span>
										<ul class="m-menu__subnav">
											<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" ><span class="m-menu__link"><span class="m-menu__link-text">Groups</span></span></li>
											<li class="m-menu__item '.$activeClassSubmenu.'" aria-haspopup="true" ><a  href="'. route('group', ['param'=>'list']) .'" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">User Groups</span></a></li>
										</ul>
									</div>
								</li>';
				endif;
				if(in_array("orders", $permission_list)):
				$activeClass = ($segment1=="orders")?"m-menu__item--open":"" ;
				$activeClassSubmenu = ($segment1=="orders" && $segment2=="list")?"m-menu__item--active":"" ;
				$permission .= '<li class="m-menu__item  m-menu__item--submenu '.$activeClass.'" aria-haspopup="true"  m-menu-submenu-toggle="hover"><a  href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-suitcase"></i><span class="m-menu__link-text">Orders</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
									<div class="m-menu__submenu "><span class="m-menu__arrow"></span>
										<ul class="m-menu__subnav">
											<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" ><span class="m-menu__link"><span class="m-menu__link-text">Orders</span></span></li>
											<li class="m-menu__item '.$activeClassSubmenu.'" aria-haspopup="true" ><a  href="'. route('orders', ['param'=>'list']) .'" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Order List</span></a></li>
										</ul>
									</div>
								</li>';
				endif;
				if(in_array("location", $permission_list)):
					$activeClass = ($segment1=="location")?"m-menu__item--open":"" ;
					$activeClassSubmenu = ($segment1=="location" && $segment2=="list")?"m-menu__item--active":"" ;
					$permission .= '<li class="m-menu__item  m-menu__item--submenu '.$activeClass.'" aria-haspopup="true"  m-menu-submenu-toggle="hover"><a  href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-suitcase"></i><span class="m-menu__link-text">Locations</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
										<div class="m-menu__submenu "><span class="m-menu__arrow"></span>
											<ul class="m-menu__subnav">
												<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" ><span class="m-menu__link"><span class="m-menu__link-text">Locations</span></span></li>
												<li class="m-menu__item '.$activeClassSubmenu.'" aria-haspopup="true" ><a  href="'. route('location', ['param'=>'list']) .'" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Add location</span></a></li>
												<li class="m-menu__item  aria-haspopup="true" ><a  href="'. route('location', ['param'=>'driver']) .'" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Driver location</span></a></li>
											</ul>
										</div>
									</li>';
					endif;
				if(in_array("discount",$permission_list)):
					$activeClass = ($segment1=="discount")?"m-menu__item--active":"" ;
					$permission .= '<li class="m-menu__item  m-menu__item--submenu '.$activeClass.'" aria-haspopup="true"  m-menu-submenu-toggle="hover"><a  href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon fa fa-money-bill-wave"></i><span class="m-menu__link-text">Discount</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
					<div class="m-menu__submenu "><span class="m-menu__arrow"></span>
						<ul class="m-menu__subnav">
							<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" ><span class="m-menu__link"><span class="m-menu__link-text">Discount</span></span></li>
							<li class="m-menu__item '.$activeClassSubmenu.'" aria-haspopup="true" ><a  href="'. route('discount', ['param'=>'add']) .'" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Add Discount</span></a></li>
							<li class="m-menu__item '.$activeClassSubmenu1.'" aria-haspopup="true" ><a  href="'. route('discount', ['param'=>'list']) .'" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Discount List</span></a></li>
						</ul>
					</div>
				</li>';	
				endif;	
			return $permission;		
}
	

