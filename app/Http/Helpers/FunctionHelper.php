<?php
namespace App\Http\Helpers;
use App\Role_has_permissions;
use Illuminate\Support\Facades\Session;
class FunctionHelper {
	
    public static function getRolePermissionList($role){
    	
		$rolePermissiom  = new Role_has_permissions();
		$permissions_detail	= $rolePermissiom->gerRolePermission($role);
		$permissionList = [];
		foreach($permissions_detail as $detail):
			$permissionList[] = $detail->slug;
		endforeach;
		return $permissionList;
	}
	
	// public static function switchWarehouse($warehouse_id=null,$name=null){
	// 	if(!$warehouse_id):
	// 		$data = Warehouse::first();
	// 		 $warehouse_session = [
	// 				'warehouse_id'=>$data->id,
	// 				'warehouse_name'=>$data->name
	// 			]; 
	// 	else:
	// 		$warehouse_session = [
	// 				'warehouse_id'=>$warehouse_id,
	// 				'warehouse_name'=>$name
	// 			];
	// 	endif;
	// 	Session::put('warehouse', $warehouse_session);
	// 	if (Session::has('warehouse')):
	// 		return true;
	// 	else:
	// 		return false;
	// 	endif;
			
	// }

}
?>