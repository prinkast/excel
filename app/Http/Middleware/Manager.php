<?php

namespace App\Http\Middleware;
use Closure;
use Auth;
use App\Http\Helpers\FunctionHelper;
class Manager
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
     //at the top
 
		function handle($request, Closure $next)
		{
			if (Auth::check() && Auth::user()->role == 'manager') :
				$persmission  = FunctionHelper::getRolePermissionList(Auth::user()->role);
				$currentroute = $request->route()->getName();
				if (in_array($currentroute,$persmission)) :
					return $next($request);		
				elseif(in_array('permission',$persmission) && $request->route()->uri=='get-permission-per-role'):
					return $next($request);
				elseif(in_array('group',$persmission) && (($request->route()->uri=='delete-recodrs') || ($currentroute=='user-group') || ($request->route()->uri=='select-user') || ($request->route()->uri=='all-status-update'))):
					return $next($request);	
				elseif(in_array('inventory',$persmission) && (($currentroute=='product-profile'))):
					return $next($request);
				elseif(in_array('box',$persmission) && (($request->route()->uri=='add-box') || ($currentroute=='box-product') || ($request->route()->uri=='save-product-to-box') || ($request->route()->uri=='remove-product-from-box') || ($currentroute=='box-driver') ||($currentroute=='box-to-box') || ($request->route()->uri=='transfer-box-to-box'))):
					return $next($request);
				elseif(in_array('store',$persmission) && (($request->route()->uri=='save-product-to-store'))):
					return $next($request);
				elseif(in_array('currency',$persmission) && (($request->route()->uri=='currency-list-by-type'))):
					return $next($request);
				else:	
					return redirect('/404');
				endif;
			elseif (Auth::check() && Auth::user()->role == 'admin') :
				$persmission  = FunctionHelper::getRolePermissionList(Auth::user()->role);
				$currentroute = $request->route()->getName();
				if (in_array($currentroute,$persmission)) :
					return $next($request);		
				elseif(in_array('permission',$persmission) && $request->route()->uri=='get-permission-per-role'):
					return $next($request);
				elseif(in_array('group',$persmission) && (($request->route()->uri=='delete-recodrs') || ($currentroute=='user-group') || ($request->route()->uri=='select-user') || ($request->route()->uri=='all-status-update'))):
					return $next($request);	
				elseif(in_array('inventory',$persmission) && (($currentroute=='product-profile'))):
					return $next($request);
				elseif(in_array('box',$persmission) && (($request->route()->uri=='add-box') || ($currentroute=='box-product') || ($request->route()->uri=='save-product-to-box') || ($request->route()->uri=='remove-product-from-box') || ($currentroute=='box-driver') ||($currentroute=='box-to-box') || ($request->route()->uri=='transfer-box-to-box'))):
					return $next($request);
				elseif(in_array('store',$persmission) && (($request->route()->uri=='save-product-to-store'))):
					return $next($request);
				elseif(in_array('currency',$persmission) && (($request->route()->uri=='currency-list-by-type'))):
					return $next($request);
				else:	
					return redirect('/404');
				endif;
			else:
				return redirect('/');
			endif;
		}
}
