<?php

namespace App\Http\Middleware;
use Auth;
use App\Http\Helpers\FunctionHelper;
use Closure;

class Checkpermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
     {
        if (Auth::check() && !empty(Auth::user())) 
        {
            $persmission  = FunctionHelper::getRolePermissionList(Auth::user()->role);
            $currentroute = $request->route()->getName();

            if (in_array($currentroute,$persmission)) {
                return $next($request);	
              }	
         else {
             return redirect('/');
             }
       }   
    }
}
