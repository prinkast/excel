<?php 
namespace App\library;
use Illuminate\Http\Request;
use \App\library\functions;
use DB;
class Functions {
    public function Fetchrow($table,$condition) {
        return $data = DB::table($table)
                ->where($condition)
                ->first();
    }

    public function GetallData($table,$condition) {
        return $data = DB::table($table)
        		->where($condition)
                ->get();
    }

    public function records($method,$table,$data){
        return DB::table($table)->$method($data);
    }

    public function updateRecord($table,$update_array,$where){
        return $store_delete = DB::table($table)->where($where)->update($update_array);
    }

    public function deleteRecord($table,$where){
        return DB::table($table)->where($where)->delete();
    }

    public function getStatus($param){
        $status = ['1'=>'Active','2'=>'Inactive','3'=>'Deleted'];
        return $status[$param];
    }

    public function uploadfile($path,$file){
        $destinationPath = public_path().'/images/'.$path;
        return $file->move($destinationPath,$file->getClientOriginalName());
    }

}

?>