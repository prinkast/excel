<?php $__env->startSection('content'); ?>

    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid mt-5">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__body kt-portlet__body--fit">
                <!--begin: Datatable -->
                <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded" id="local_data" style="">
					<form class="kt-form" method="post" id="programs_assign">
						<div class="kt-portlet__body " >
							<div class="row">
                                <div class="col-md-7">
                                    
                                    <div class="row">
                                           <input type="hidden" name="program_id" value="">
                                        <div class="form-group col-md-6">
                                            <label>Select Category</label>
                                            <select class="form-control" name="category" id="select_cat">
                                                <option selected disabled>Select</option>
                                                <?php if(isset($categoryList)): ?>
                                                    <?php $__currentLoopData = $categoryList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($category['id']); ?>"><?php echo e($category['name']); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php endif; ?>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Select Programs</label>
                                            <select class="form-control" name="program" id="select_pro">
                                                
                                            </select>
                                        </div>
                                    </div>
                                           
                                    <div class="kt-portlet__foot">
                                        <div class="kt-form__actions">
                                            <button type="button" class="btn btn-primary" id="assign_btn">Assign</button>
                                            <button type="reset" class="btn btn-secondary">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
						</div>
					</form>                                    
                </div>
                <!--end: Datatable -->
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>
                    


<?php echo $__env->make('layouts.form', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\tut_manage\resources\views/instructor/request.blade.php ENDPATH**/ ?>