<!DOCTYPE html>
<html lang="<?php echo e(app()->getLocale()); ?>" >
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
        <meta charset="utf-8" />
        <title>Dashboard</title>
        <meta name="description" content="Pagination options datatables examples"> 
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
		<meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
		<link href="/public/flaticon/flaticon.css" rel="stylesheet" type="text/css" />
	    <link href="/public/flaticon2/flaticon.css" rel="stylesheet" type="text/css" />
	    <link href="/public/css/all.min.css" rel="stylesheet" type="text/css" />
	    <link href="/public/css/style.bundle.css" rel="stylesheet" type="text/css" />
	    <link href="/public/css/datatables.bundle.css" rel="stylesheet" type="text/css" />
	    <link rel="shortcut icon" href="public/images/favicon.ico" />	
	    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

	    <link rel="stylesheet" type="text/css" href="/public/css/<?php echo e(Request :: segment(1)); ?>.css">
	    <!-- Latest compiled and minified CSS -->
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />

</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--fixed kt-page--loading">
	<?php if(Auth::user()->role == '1'): ?>
    	<?php echo $__env->make('inc.admin.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php else: ?>
    	<?php echo $__env->make('inc.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php endif; ?>
    <?php echo $__env->make('inc.admin.sub_header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php echo $__env->yieldContent('content'); ?>
    <?php echo $__env->make('inc.admin.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>


</body>
<script src="/public/js/jquery.min.js" type="text/javascript"></script>
<script src="/public/js/popper.js" type="text/javascript"></script>
<script src="/public/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/public/js/js.cookie.js" type="text/javascript"></script>
<script src="/public/js/moment.min.js" type="text/javascript"></script>
<script src="/public/js/perfect-scrollbar.js" type="text/javascript"></script>
<script src="/public/js/sticky.js" type="text/javascript"></script>
<script src="/public/js/scripts.bundle.js" type="text/javascript"></script>
<script type="text/javascript" src="/public/js/jquery.validate.js"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<script src="/public/js/<?php echo e(Request :: segment(1)); ?>.js" type="text/javascript"></script>
</html>




<?php /**PATH C:\xampp\htdocs\tut_manage\resources\views/layouts/form.blade.php ENDPATH**/ ?>