<?php $__env->startSection('content'); ?>


    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid mt-5">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__body kt-portlet__body--fit">
                <!--begin: Datatable -->
                <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded" id="local_data" style="">
                    <form class="kt-form" method="post" id="programs_form">
                        <div class="kt-portlet__body " >
                            <div class="row">
                                <div class="col-md-7">
                                    <input type="hidden" name="program_id" value="<?php echo e(isset($programsList['id'])? $programsList['id'] : ''); ?>">
                                    <input type="hidden" name="method" value="<?php echo e(isset($edit)?'update':'add'); ?>">
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="exampleSelect1">Select Category</label>
                                            <select class="form-control" name="category">
                                                <?php if(isset($categoryList)): ?>
                                                    <?php $__currentLoopData = $categoryList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($category['id']); ?>" <?php echo (isset($programsList['category_id']) && $programsList['category_id'] ==$category['id'])?'selected' : '' ?>><?php echo e($category['name']); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php endif; ?>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Program Name</label>
                                            <input type="text" class="form-control" aria-describedby="nameHelp" placeholder="Enter program Name" name="program_name" value="<?php echo e(isset($programsList['title'])? $programsList['title'] : ''); ?>">            
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Cost</label>
                                            <input type="text" class="form-control" aria-describedby="nameHelp" name="cost"  value="<?php echo e(isset($programsList['cost'])? $programsList['cost'] : ''); ?>">            
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Program length</label>
                                            <input type="text" class="form-control" aria-describedby="nameHelp" name="length"  value="<?php echo e(isset($programsList['length'])? $programsList['length'] : ''); ?>">            
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Program validity</label>
                                            <input type="text" class="form-control" aria-describedby="nameHelp" name="validity" value="<?php echo e(isset($programsList['validity'])? $programsList['validity'] : ''); ?>">            
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Write up</label>
                                            <input type="text" class="form-control" aria-describedby="nameHelp" name="writeup"value="<?php echo e(isset($programsList['write_up'])? $programsList['write_up'] : ''); ?>">            
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Pass percent</label>
                                            <input type="text" class="form-control" aria-describedby="nameHelp" name="percent" value="<?php echo e(isset($programsList['pass_percent'])? $programsList['pass_percent'] : ''); ?>">            
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="exampleSelect1">Need permit</label>
                                            <select class="form-control" name="permit">
                                                <option value="1" <?php echo (isset($programsList['need_permit']) && $programsList['need_permit'] =='1')?'selected' : '' ?>>Yes</option>
                                                <option value="2" <?php echo (isset($programsList['need_permit']) && $programsList['need_permit'] =='2')?'selected' : '' ?>>No</option>
                                            </select>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label for="exampleSelect1">Status</label>
                                            <select class="form-control" name="status">
                                                <option value="1" <?php echo (isset($programsList['status']) && $programsList['status'] =='1')?'selected' : '' ?>>Active</option>
                                                <option value="2" <?php echo (isset($programsList['status']) && $programsList['status'] =='2')?'selected' : '' ?>>Inactive</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Color</label>
                                            <input type="color" class="form-control" aria-describedby="nameHelp" name="color" value="<?php echo e(isset($programsList['colour'])? $programsList['colour'] : '#ff0000'); ?>">            
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>General message</label>
                                            <textarea rows="5" cols="15" class="form-control" aria-describedby="nameHelp" name="message" value="<?php echo e(isset($programsList['general_message'])? $programsList['general_message'] : ''); ?>"></textarea>           
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Required documents</label>
                                            <select class="form-control" name="documents[]" id="select_doc" multiple="multiple">
                                                <?php if(isset($documents)): ?>
                                                    <?php $__currentLoopData = $documents; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $document): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($document['id']); ?>" <?php echo (isset($document['document_id']) && $document['document_id'] == $document['id'] && $document['cat_pro_id'] == $programsList['id'])?'selected' : '' ?>><?php echo e($document['name']); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php endif; ?>
                                            </select>
                                        </div> 
                                    </div>
                                    <input type="hidden" name="data" value="program">   
                                    <div class="kt-portlet__foot">
                                        <div class="kt-form__actions">
                                            <button type="submit" class="btn btn-primary" id="<?php echo e(isset($edit)?'update':'add'); ?>">Submit</button>
                                            <a type="button" href="<?php echo e(route('program',['param'=>'list'])); ?>"class="btn btn-secondary">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>                                    
                </div>
                <!--end: Datatable -->
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
                    
<?php echo $__env->make('layouts.form', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\tut_manage\resources\views/admin/programs/add.blade.php ENDPATH**/ ?>