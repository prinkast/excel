<?php $__env->startSection('content'); ?>



                    <!-- sidebar and header ends here -->

                    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid mt-5">

                        <div class="kt-portlet kt-portlet--mobile">
                            <div class="kt-portlet__body kt-portlet__body--fit">
                                <!--begin: Datatable -->
                                <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded" id="local_data" style="">

                                    <form class="kt-form" method="post" action=" " id="add_user">
                                        <div class="kt-portlet__body " >
                                            <div class="row">

                                                <div class="col-md-3">
                                                    <div class="main-div">
                                                        <div class="input_feild">
                                                        <input type="file" name="avatar" class="my-form" id="default_file"  onchange="Showimage(this);"/>
                                                        <div class="onclick_image">
                                                            <?php 
                                                            if(isset($instructorData['image']) && $instructorData['image'] != ''){
                                                                $image = $instructorData['image'];
                                                            }else{
                                                                $image = '300_25.jpg';
                                                            }

                                                            ?>
                                                            <img src="/public/images/instructor/<?php echo e($image); ?>" class="my_profile_image" id="my_profile_image">
                                                        </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="hidden" name="instructor_id" value="<?php echo e(isset($instructorData['id'])? $instructorData['id'] : ''); ?>">
                                                    <input type="hidden" name="method" value="<?php echo e(isset($instructorData['edit'])?'update':''); ?>">
                                                    <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <label>First Name</label>
                                                        <input type="text" class="form-control" aria-describedby="nameHelp" placeholder="Enter First Name" name="firstname" value="<?php echo e(isset($instructorData['firstname'])? $instructorData['firstname'] : ''); ?>">            
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label>Last Name</label>
                                                        <input type="text" class="form-control" aria-describedby="nameHelp" placeholder="Enter Name" name="lastname" value="<?php echo e(isset($instructorData['lastname'])? $instructorData['lastname'] : ''); ?>">            
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label>Email address</label>
                                                        <input type="email" class="form-control" aria-describedby="emailHelp" placeholder="Enter email" name="email" value="<?php echo e(isset($instructorData['email'])? $instructorData['email'] : ''); ?>">            
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label>Phone</label>
                                                        <input type="text" class="form-control" placeholder="Enter Phone number" name="phone" value="<?php echo e(isset($instructorData['phone'])? $instructorData['phone'] : ''); ?>">            
                                                    </div>
                                                    <?php if(Auth::user()->role =='1'): ?>
                                                    <div class="form-group col-md-6">
                                                        <label for="exampleSelect1">Status</label>
                                                        <select class="form-control" name="status">
                                                            <option value="1" <?php echo (isset($instructorData['status']) && $instructorData['status'] =='1')?'selected' : '' ?>>Active</option>
                                                            <option value="2" <?php echo (isset($instructorData['status']) && $instructorData['status'] =='2')?'selected' : '' ?>>Inactive</option>
                                                        </select>
                                                    </div>
                                                    <?php else: ?>
                                                    <?php
                                                        $object = new \App\library\functions;
                                                    ?>
                                                        <div class="form-group col-md-6">
                                                        <label>Status :  <?php echo e($object->getStatus($instructorData['status'])); ?> </label>
                                                        <input type="hidden" class="form-control" name="phone" value="<?php echo e($instructorData['phone']); ?>">            
                                                    </div>
                                                    <?php endif; ?>
                                                </div>
                                                    <input type="hidden" name="data" value="instructor">
                                                    <div class="kt-portlet__foot">
                                                        <div class="kt-form__actions">
                                                            <button type="submit" class="btn btn-primary" id="add">Submit</button>
                                                            <a type="button" href="<?php echo e(route('instructor',['param'=>'list'])); ?>" class="btn btn-secondary">Cancel</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            
                                        </div>
                                        </div>
                                    </form>                                    
                                </div>
                                <!--end: Datatable -->
                            </div>
                        </div>
                    </div>
                    <?php $__env->stopSection(); ?>
                    
<?php echo $__env->make('layouts.form', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\tut_manage\resources\views/admin/instructor/add.blade.php ENDPATH**/ ?>