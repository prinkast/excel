<?php $__env->startSection('content'); ?>

<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

	<!-- begin:: Subheader -->
	<div class="kt-subheader   kt-grid__item" id="kt_subheader">
		<div class="kt-container  kt-container--fluid ">
			<div class="kt-subheader__main">
				<h3 class="kt-subheader__title">
				Admin Dashboard </h3>
				<span class="kt-subheader__separator kt-hidden"></span>
				<!-- <div class="kt-subheader__breadcrumbs">
					<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
					<span class="kt-subheader__breadcrumbs-separator"></span>
					<a href="" class="kt-subheader__breadcrumbs-link">
					</a> -->

					<!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
				<!-- </div> -->
			</div>

		</div>
	</div>

	<!-- end:: Subheader -->


	<!-- sidebar and header ends here -->

       <section class="dashboard-fileds" id="team">
    <div class="container">	
        <div class="row">
			<div class="col-md-4 col-offset-md-4">
				<h2>Import file</h2>
				<form name="" method="Post" action="" enctype="multipart/form-data">
					  <?php echo csrf_field(); ?>		
					
					<div class="form-group">
						<label for="email">Upload File:</label>
						<input type="file" name="upload_file" class="form-control"  required>
					</div>			
					<div class="form-submit">
					<input type="submit" name="submit" class="btn btn-default" value="Submit">
					</div>
				</form>
			
			</div>
		</div>
	</div>
</section>


		<?php $__env->stopSection(); ?>                    
<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\tut_manage\resources\views/admin/users/dashboard.blade.php ENDPATH**/ ?>