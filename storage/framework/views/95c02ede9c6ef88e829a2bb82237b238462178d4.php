<?php $__env->startSection('content'); ?>

    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid mt-2">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__body kt-portlet__body--fit">
                <!--begin: Datatable -->
                <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded" id="local_data" style="">
                    <form class="kt-form" method="post" id="category_form">
                        <div class="kt-portlet__body " >
                            <div class="row">
                                <div class="col-md-7">
                                    <input type="hidden" name="category_id" value="<?php echo e(isset($category['id'])? $category['id'] : ''); ?>">
                                    <input type="hidden" name="method" value="<?php echo e(isset($edit)?'update':'add'); ?>">
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label>Category Name</label>
                                            <input type="text" class="form-control" aria-describedby="nameHelp" placeholder="Enter Category Name" name="cat_name" value="<?php echo e(isset($category['name'])? $category['name'] : ''); ?>">            
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Time valid</label>
                                            <input type="text" class="form-control" aria-describedby="nameHelp" name="time" id="datepicker" value="<?php echo e(isset($category['time_valid'])? $category['time_valid'] : ''); ?>">            
                                        </div>
                                        
                                        <div class="form-group col-md-6">
                                            <label for="exampleSelect1">Status</label>
                                            <select class="form-control" name="status">
                                                <option value="1" <?php echo (isset($category['status']) && $category['status'] =='1')?'selected' : '' ?>>Active</option>
                                                <option value="2" <?php echo (isset($category['status']) && $category['status'] =='2')?'selected' : '' ?>>Inactive</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Required documents</label>
                                            <select class="form-control" name="documents[]" id="select_doc" multiple="multiple">
                                                <?php if(isset($documents)): ?>
                                                    <?php $__currentLoopData = $documents; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $document): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($document['id']); ?>" <?php echo (isset($document['document_id']) && $document['document_id'] ==$document['id'])?'selected' : '' ?>><?php echo e($document['name']); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php endif; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <input type="hidden" name="data" value="category">       
                                    <div class="kt-portlet__foot">
                                        <div class="kt-form__actions">
                                            <button type="submit" class="btn btn-primary" id="<?php echo e(isset($edit)?'update':'add'); ?>">Submit</button>
                                            <a type="button" href="<?php echo e(route('category',['param'=>'list'])); ?>" class="btn btn-secondary">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>                                    
                </div>
                <!--end: Datatable -->
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
                    
<?php echo $__env->make('layouts.form', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\tut_manage\resources\views/admin/category/add.blade.php ENDPATH**/ ?>