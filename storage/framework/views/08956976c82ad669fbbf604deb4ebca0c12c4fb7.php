<?php $__env->startSection('content'); ?>

    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid mt-2">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__body kt-portlet__body--fit">
                <!--begin: Datatable -->
                <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded" id="local_data" style="">
                    <form class="kt-form" method="post" id="category_form">
                        <div class="kt-portlet__body " >
                            <div class="row">
                                <div class="col-md-7">
                                    <input type="hidden" name="agency_id" value="">
                                    <input type="hidden" name="method" value="<?php echo e(isset($edit)?'update':'add'); ?>">
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label>Agency Name</label>
                                            <input type="text" class="form-control" aria-describedby="nameHelp" placeholder="Enter Agency Name" name="agency_name" value="">            
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Email</label>
                                            <input type="text" class="form-control" aria-describedby="nameHelp" placeholder="Enter Agency Name" name="email" value="">            
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Phone</label>
                                            <input type="text" class="form-control" aria-describedby="nameHelp" name="phone" value="">            
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Address</label>
                                            <input type="text" class="form-control" aria-describedby="nameHelp" placeholder="Enter Agency Name" name="address" value="">            
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Location</label>
                                            <input type="text" class="form-control" aria-describedby="nameHelp" name="location" value="">            
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>City</label>
                                            <input type="text" class="form-control" aria-describedby="nameHelp" name="logo" value="">            
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Zip code</label>
                                            <input type="text" class="form-control" aria-describedby="nameHelp" name="zipcode" value="">            
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="exampleSelect1">Status</label>
                                            <select class="form-control" name="status">
                                                <option value="1" <?php echo (isset($category['status']) && $category['status'] =='1')?'selected' : '' ?>>Active</option>
                                                <option value="2" <?php echo (isset($category['status']) && $category['status'] =='2')?'selected' : '' ?>>Inactive</option>
                                            </select>
                                        </div>
                                    </div>
                                    <input type="hidden" name="data" value="category">       
                                    <div class="kt-portlet__foot">
                                        <div class="kt-form__actions">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <a type="button" href="<?php echo e(route('agency',['param'=>'list'])); ?>" class="btn btn-secondary">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>                                    
                </div>
                <!--end: Datatable -->
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
                    
<?php echo $__env->make('layouts.form', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\tut_manage\resources\views/admin/agency/add.blade.php ENDPATH**/ ?>