<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUploadFileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('upload_file', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->string('Date');
			$table->string('Track');
			$table->string('Type');
			$table->string('Distance');
			$table->string('Horse');
			$table->string('Jockey');
			$table->string('Trainer');
			$table->string('SP Fav');
			$table->string('Industry SP');
			$table->string('Betfair SP');
			$table->string('IP Min');
			$table->string('IP Max');
			$table->string('Pre Min');
			$table->string('Pre Max');
			$table->string('Pred Industry SP');
			$table->string('Place');
			$table->string('Runners');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('upload_file');
    }
}
