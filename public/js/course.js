var base_url = window.location.origin;
$(document).ready(function(){
	$("#course_table").DataTable({
		paging: true,
		searching: true,
    	ordering:  true,
    	dom: 'Bfrtip',
        buttons: [
           'copy', 'csv', 'excel', 'pdf', 'print'
        ],
	});
});

$.validator.setDefaults({
	submitHandler: function() { 
		$.ajaxSetup({
		  headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		  }
		});


	 	jQuery.ajax({
          	url: base_url+'/ajax/save',
          	method: 'post',
          	data: {
          		course_id :$("input[name=course_id]").val(),
          		course_name :$("input[name=course_name]").val(),
				program :$("select[name=program]").val(),
				instructor :$("select[name=instructor]").val(),
				duration:$("input[name=duration]").val(),
				agency:$("input[name=agency]").val(),
				start:$("input[name=start]").val(),
				end:$("input[name=end]").val(),
				scope:$("select[name=scope]").val(),
				location:$("input[name=location]").val(),
				notes:$("input[name=notes]").val(),
				permit:$("select[name=permit]").val(),
				status:$("select[name=status]").val(),
				instructor_notes:$("input[name=instructor_notes]").val(),
				data:$("input[name=data]").val(),
          	},
          	success: function(response){
			  	if(response.status == true){
					$("div.kt-form__actions").after("<p class='msg text-success'>"+response.message+"</p>");
					$("#course_form").trigger("reset");
					setTimeout(function(){ window.location.href = '/course/list'; }, 1000);
					
				}else{
					$("div.kt-form__actions").after("<p class='msg text-danger'>"+response.message+"</p>");
				}
			
          	},
		});
	}
});


$(document).ready(function() {
	// validate signup form on keyup and submit
	$("#course_form").validate({
		rules: {
			duration:'required',
			agency:'required',
			start:'required',
			end:'required',
			scope:'required',
			location:'required',
			notes:'required',
			course_name:'required',
			program:'required',
		},
		messages: {
			duration: "Please enter Duration",
			agency:'Please enter agency',
			start:'Please enter start date',
			end:'Please enter end date',
			scope:'Please enter Private value',
			location:'Please enter location',
			notes:'Please enter notes',
			course_name:'Please enter course name',
			program:'Please select program',
		},
		
	});	
});	

$( function() {
    $( "#start , #end" ).datepicker();
} );

function Delete(id){
	$.ajaxSetup({
        headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
	});
	$.ajax({
		url:base_url+"/course/delete/"+id,
		type:"post",
		success:function(response){
			if(response.status == true){
				location.reload();
			}else{
				
			}
		},

	})
}