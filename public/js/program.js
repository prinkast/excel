var base_url = window.location.origin;

$(document).ready(function(){
	$("#program_table").DataTable({
		paging: true,
		searching: true,
    	ordering:  true,
    	dom: 'Bfrtip',
        buttons: [
           'copy', 'csv', 'excel', 'pdf', 'print'
        ],
	});
})


$.validator.setDefaults({
	submitHandler: function() { 
		$.ajaxSetup({
		  headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		  }
		});
	 	jQuery.ajax({
          	url: base_url+'/ajax/save',
          	method: 'post',
          	data: $("#programs_form").serialize(),
          	success: function(response){
			  	if(response.status == true){
					$("div.kt-portlet__foot").after("<p class='msg text-success'>"+response.message+"</p>");
					$("#programs_form").trigger("reset");
					setTimeout(function(){ window.location.href = '/program/list'; }, 1000);
					
				}else{
					$("div.kt-portlet__foot").after("<p class='msg text-danger'>"+response.message+"</p>");
				}
			
          	},
		});
	}
});


$(document).ready(function() {
	// validate signup form on keyup and submit
	$("#programs_form").validate({
		rules: {
			program_name: "required",
			cost: 'required',
			length:'required',
			validity:'required',
			writeup:'required',
			percent:'required',
			percent:'required',
			category:'required',
		},
		messages: {
			name: "Please enter program name",
			cost: "Please enter a cost",
			length: "Please enter length",
			validity:'Please enter validity',
			writeup:'Please enter writeup',
			percent:'Please enter percent',
			percent:'Please enter percent',
			category:'Please select category',
		},
		
	});	
});	

function Delete(id){
	$.ajaxSetup({
        headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
	});
	$.ajax({
		url:base_url+"/program/delete/"+id,
		type:"post",
		success:function(response){
			if(response.status == true){
				location.reload();
			}else{
				
			}
		},

	})
}
$(document).ready(function() {
    $('.js-example-basic-multiple , #select_instructor').select2();
});
$("#assign_btn").on('click',function(e){
	$.ajaxSetup({
        headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
	});
	$.ajax({
		url:base_url+"/assign/program/save",
		type:"post",
		data: $("#programs_assign").serialize(),

		success:function(response){
			if(response.status == true){
				$("div.kt-portlet__foot").after("<p class='msg text-success'>"+response.message+"</p>");
				$("#programs_form").trigger("reset");
				setTimeout(function(){ window.location.href = '/program/list'; }, 1000);
				
			}else{
				
			}
		},

	})
	e.stopImmediatePropagation();
})


function RemoveInstructor(instructorid,program_id){
	$.ajaxSetup({
        headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
	});
	$.ajax({
		url:base_url+"/assign/program/delete",
		type:"post",
		data:{
			instructorid:instructorid,
			program:program_id,
		},
		success:function(response){
			if(response.status == true){
				location.reload();
			}else{
				
			}
		},

	})
}

$(document).ready(function() {
    $('#select_doc').select2();
});

$(document).ready(function() {
    $('#select_doc').select2();

    $("#select_doc").on("select2:unselect", function (e) {
        var value =   e.params.data.id;
        if($('input[name=category_id]').val() !=''){
	        $.ajaxSetup({
		        headers: {
		              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
			});
			$.ajax({
				url:base_url+"/remove/doc",
				type:"post",
				data:{
					id:value,
					category:$('input[name=program_id]').val(),
				},
				success:function(response){
					if(response.status == true){
						//location.reload();
					}else{
						
					}
				},

			})
		}
        e.stopImmediatePropagation(); 
    });
});