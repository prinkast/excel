var base_url = window.location.origin;
//$("#add").on('click',function(){
$("#add_user").submit(function(e) {
e.preventDefault(); 
	var firstname = $("input[name=firstname]").val();
	var lastname = $("input[name=lastname]").val();
	var email = $("input[name=email]").val();
	var phone = $("input[name=phone]").val();
	var instructor_id = $("input[name=instructor_id]").val();
	var action = $("input[name=method]").val();
	var formData = new FormData(this);
	var has_error= 0;
	$("p.error").remove();
	
	if(Validate() == true){

		$.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
	    });
		$.ajax({
			url:base_url +'/ajax/save',
			type:"post",
			data:formData,
			processData: false,  // tell jQuery not to process the data
       		contentType: false,  // tell jQuery not to set contentType
			success:function(response){
				if(response.status == true){
					alert('successfully added');
					window.location.href = '/instructor/list';
				}else{
					alert('something went wrong');
				}
			}

		})
		}
})

function checkEmail(email){
	var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if(!regex.test(email)) {
		return false;
	}else{
		return true;
	}
}


function Validate(){
	var firstname = $("input[name=firstname]").val();
	var lastname = $("input[name=lastname]").val();
	var email = $("input[name=email]").val();
	var phone = $("input[name=phone]").val();
	var has_error= 0;
	$("p.error").remove();
	if(firstname == ''){
		has_error = 1;
		$("input[name=firstname]").after("<p class='error text-danger'>Please enter first name</p>");
		  
	}
	if(lastname == ''){
		has_error = 1;
		$("input[name=lastname]").after("<p class='error text-danger'>Please enter last name</p>");
		
	}
	if(email == ''){
		has_error = 1;
		$("input[name=email]").after("<p class='error text-danger'>Please enter email</p>");
		 
	}else{
		var response = checkEmail(email);
		if(response == false){
			has_error = 1;
			$("input[name=email]").after("<p class='error text-danger'>Please enter valid email address</p>");
			
		}
		console.log($("input[name=method]").val());
		if($("input[name=method]").val() !='update'){
			var res = Emailexists(email);
			if(res == false){
				has_error = 1;
				$("input[name=email]").after("<p class='error text-danger'>Email already exists</p>");
			}
		
		}
		
	}
	
	if(has_error ==1){
		return false;
	}else{
		return true;
	}
}

$(document).ready(function(){
	var table =	$("#instructor_table").DataTable({
		paging: true,
		searching: true,
    	ordering:  true,
    	dom: 'Bfrtip',
        buttons: [
           'copy', 'csv', 'excel', 'pdf', 'print'
        ],
	});
table.buttons().container()
        .appendTo( '#droplist' );

})

	$("input").on('keyup',function(){
		$(this).parent().children("p.error").remove();
	})
function Showimage(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#my_profile_image')
                .attr('src', e.target.result)
                .width(150)
                .height(200);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function Emailexists(email){
	var ajaxresponse =0;
	$.ajaxSetup({
      	headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    var error = 0;
	$.ajax({
		url:base_url+"/checkemail",
		type:"post",
		data:{'email':email},
		async: false,
		success:function(response){
			
			if(response.status == true){
				ajaxresponse = 1;
			}
		}
	});
	if(ajaxresponse == 1){
		return false;
	}else{
		return true;
	}
}

function Delete(id){
	$.ajaxSetup({
        headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
	});
	$.ajax({
		url:base_url+"/instructor/delete/"+id,
		type:"post",
		success:function(response){
			if(response.status == true){
				location.reload();
			}else{
				
			}
		},

	})
}

$("#change_pass").on('submit',function(e){
	e.preventDefault();
	has_error = 0;
	$("p.error").remove();
	if($("input[name=current]").val() == ''){
		$("input[name=current]").after("<p class='error text-danger'>Please enter current password</p>");
		has_error = 1;
	}
	if($("input[name=new_pass]").val()==''){
	 	$("input[name=new_pass]").after("<p class='error text-danger'>Please enter new password</p>");
	 	has_error = 1;
	}
	if($("input[name=confirm_pass]").val()==''){
		$("input[name=confirm_pass]").after("<p class='error text-danger'>Please enter confirm password</p>");
		has_error = 1;
	}
	if($("input[name=confirm_pass]").val() !='' && $("input[name=new_pass]").val() !=''){
		if($("input[name=confirm_pass]").val() != $("input[name=new_pass]").val()){
			$("input[name=confirm_pass]").after("<p class='error text-danger'>New password and confirm password does not match</p>");
			has_error = 1;
		}
	}
	var formData = new FormData(this);
	if(has_error== 0){
		$.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
	    });
		$.ajax({
			url:base_url+"/update-password",
			type:"post",
			data:formData,
			processData: false,  // tell jQuery not to process the data
       		contentType: false,  // tell jQuery not to set contentType
			success:function(response){
				if(response.status == true){
					$("#kt_login_signin_submit").after("<p class='text-success'>"+ response.message +"<p>");
					
				}else{
					$("#kt_login_signin_submit").after("<p class='error text-danger'>"+ response.message +"<p>");
				}
			}

		})
	}else{
		return false;
	}
})

$("#select_cat").on('change',function(){
	
	$.ajaxSetup({
        headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
	});
	$.ajax({
		url:base_url+"/getData",
		type:"post",
		data:{
			param:'category_id',
			param_val:$("#select_cat").val(),
			table:'program',
		},
		success:function(response){
			if(response.status == true){
				var option = '';
				$.each(response.message, function (key, data) {
					$('#select_pro').append($('<option>', { 
				        value: data.id,
				        text : data.title 
				    }));
					//option += "<option value=" +data.id+ ">"+data.title+"</option>"; 
				});
				//$("#select_cat").append(option);
			}else{
				
			}
		},

	})
})