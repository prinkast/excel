var base_url = window.location.origin;
//$("#add").on('click',function(){
$("#category_form").submit(function(e) {
e.preventDefault(); 

	var name = $("input[name=cat_name]").val();
	var time_valid = $("input[name=time]").val();
	var status = $("select[name=status]").val();
	var has_error= 0;
	var formData = new FormData(this);
	$("p.error").remove();
	
	if(Validate() == true){

		$.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
	    });
		$.ajax({
			url:base_url+"/ajax/save",
			type:"post",
			data:formData,
			processData: false,  // tell jQuery not to process the data
       		contentType: false,  // tell jQuery not to set contentType
			success:function(response){
				if(response.status == true){
					$("div.kt-portlet__foot").after("<p class='msg'>"+response.message+"</p>");
					$("#category_form").trigger("reset");
					setTimeout(function(){ window.location.href = '/category/list'; }, 1000);
					
				}else{
					$("div.kt-portlet__foot").after("<p class='msg'>"+response.message+"</p>");
				}
			}

		})
		e.stopImmediatePropagation();
	}
})



function Validate(){
	var name = $("input[name=cat_name]").val();
	var time_valid = $("input[name=time]").val();
	var has_error= 0;
	$("p.error").remove();
	if(name == ''){
		has_error = 1;
		$("input[name=cat_name]").after("<p class='error text-danger'>Please enter category name</p>");
		  
	}
	if(time_valid == ''){
		has_error = 1;
		$("input[name=time]").after("<p class='error text-danger'>Please enter time</p>");
		
	}
	if(has_error ==1){
		return false;
	}else{
		return true;
	}
}

$(document).ready(function(){
	$("#category_table").DataTable({
		paging: true,
		searching: true,
    	ordering:  true,
    	dom: 'Bfrtip',
        buttons: [
           'copy', 'csv', 'excel', 'pdf', 'print'
        ],
	});

})



$( function() {
    $( "#datepicker" ).datepicker();
  } );

function Delete(id){
	$.ajaxSetup({
        headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
	});
	$.ajax({
		url:base_url+"/category/delete/"+id,
		type:"post",
		success:function(response){
			if(response.status == true){
				location.reload();
			}else{
				
			}
		},

	})
}
$(document).ready(function() {
    $('#select_doc').select2();

    $("#select_doc").on("select2:unselect", function (e) {
        var value =   e.params.data.id;
        if($('input[name=category_id]').val() !=''){
	        $.ajaxSetup({
		        headers: {
		              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
			});
			$.ajax({
				url:base_url+"/remove/doc",
				type:"post",
				data:{
					id:value,
					category:$('input[name=category_id]').val(),
				},
				success:function(response){
					if(response.status == true){
						//location.reload();
					}else{
						
					}
				},

			})
		}
        e.stopImmediatePropagation(); 
    });
});


