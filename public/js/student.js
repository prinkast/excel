var base_url = window.location.origin;

$(document).ready(function(){
	$("#program_table").DataTable({
		paging: true,
		searching: true,
    	ordering:  true,
    	dom: 'Bfrtip',
        buttons: [
           'copy', 'csv', 'excel', 'pdf', 'print'
        ],
	});
})


$.validator.setDefaults({
	submitHandler: function() { 

		$.ajaxSetup({
		  headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		  }
		});
	 	jQuery.ajax({
          	url: base_url+'/ajax/save',
          	method: 'post',
          	data: $("#student_form").serialize(),
          	success: function(response){
			  	if(response.status == true){
					$("div.kt-portlet__foot").after("<p class='msg text-success'>"+response.message+"</p>");
					$("#programs_form").trigger("reset");
					setTimeout(function(){ window.location.href = '/student/create'; }, 1000);
					
				}else{
					$("div.kt-portlet__foot").after("<p class='msg text-danger'>"+response.message+"</p>");
				}
			
          	},
		});
	}
});


$(document).ready(function() {
	// validate signup form on keyup and submit
	$("#student_form").validate({
		rules: {
			firstname: "required",
			lastname: 'required',
			email:'required',
			phone:'required',
			city:'required',
			postal_code:'required',
			birth_city:'required',
		},
		messages: {
			firstname: "Please enter first name",
			lastname: "Please enter last name",
			email: "Please enter email",
			phone:'Please enter phone number',
			city:'Please enter city',
			postal_code:'Please enter postal code',
			birth_city:'Please enter birth city',
		},
		
	});	
});	

function Delete(id){
	$.ajaxSetup({
        headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
	});
	$.ajax({
		url:base_url+"/program/delete/"+id,
		type:"post",
		success:function(response){
			if(response.status == true){
				location.reload();
			}else{
				
			}
		},

	})
}
$(document).ready(function() {
    $('.js-example-basic-multiple , #select_instructor').select2();
});
$("#assign_btn").on('click',function(e){
	$.ajaxSetup({
        headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
	});
	$.ajax({
		url:base_url+"/assign/program/save",
		type:"post",
		data: $("#programs_assign").serialize(),

		success:function(response){
			if(response.status == true){
				$("div.kt-portlet__foot").after("<p class='msg text-success'>"+response.message+"</p>");
				$("#programs_form").trigger("reset");
				setTimeout(function(){ window.location.href = '/program/list'; }, 1000);
				
			}else{
				
			}
		},

	})
	e.stopImmediatePropagation();
})


function RemoveInstructor(instructorid,program_id){
	$.ajaxSetup({
        headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
	});
	$.ajax({
		url:base_url+"/assign/program/delete",
		type:"post",
		data:{
			instructorid:instructorid,
			program:program_id,
		},
		success:function(response){
			if(response.status == true){
				location.reload();
			}else{
				
			}
		},

	})
}